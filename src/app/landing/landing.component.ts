import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { AlertService } from '../services/alert.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';
import { EasingLogic } from 'ng2-page-scroll';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  user: any;
  submitted: boolean = false;
  baseUrl: string = environment.baseUrl;
  requestedContactForm: FormGroup;
  modalTitle: string;
  modalMode: string;

  public myEasing: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
      // easeInOutExpo easing
      if (t === 0) {
        return b;
      }
      if (t === d) {
        return b + c;
      }
      if ((t /= d / 2) < 1) {
        return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      }
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
  };

  constructor(
    @Inject(DOCUMENT) public document: any, 
    public zone: NgZone,
    public router : Router, 
    public formBuilder: FormBuilder, 
    public modalService: NgbModal,
    public http: HttpClient,
    public alertService: AlertService
  ) { 
    zone.runOutsideAngular(() => {
      window.addEventListener('scroll', () => {
        const number = Math.min(this.document.body.scrollTop / 50, 1);
      });
    });
  }

  ngOnInit() {
    this.requestedContactForm = this.formBuilder.group({
      contacttype: [''],
      fullname: ['', [Validators.required,Validators.pattern(/^[a-zA-Z ]+$/)]],
      email: ['', Validators.email],
      businessname: [''],
      requirements: [''],
      contactno: ['',Validators.required],
      });
  }

  

  openRequestedContact(requestedContactContent,type: string) {
    if (type == 'freedemo') {
      this.requestedContactForm.patchValue({ contacttype: 'freedemo' });
      this.modalTitle = "Request Demo";
      this.modalMode = 'demo';
    } else {
      this.requestedContactForm.patchValue({ contacttype: 'getstarted' });
      this.modalTitle = "Sign Up";
      this.modalMode = 'signup';
    }

    this.modalService.open(requestedContactContent, { centered: true }).result.then((result) => {

    }, (reason) => { 
      this.requestedContactForm.reset({ 
      contacttype:"",
      fullname : "",
      email : "" ,
      businessname  : "",
      requirements : "",
      contactno : "" });
      this.modalTitle = "";
      this.modalMode = "";
    });
    this.submitted = false;
  }
  
  
  get form() { return this.requestedContactForm.controls; }

  onRequest(){
    this.submitted = true;
    if (this.requestedContactForm.invalid) {
      return;
    }
    // console.log(this.requestedContactForm.value);
    this.http.post(this.baseUrl + '/enquiry/addenquiry', this.requestedContactForm.value).subscribe(res =>{
      // console.log(res);
      if (res['success']) {
        this.alertService.success('Enquiry sent successfully!', false);
      } else {
        // this.alertService.error('Something went wrong!', false);
      }
    }, error =>{
        // this.alertService.error('Error!', false);
    });
    this.requestedContactForm.reset({ 
      contacttype: "",
      fullname : "",
      email : "" ,
      businessname  : "",
      requirements : "",
      contactno : "" });
    this.submitted = false;
    this.modalTitle = "";
    this.modalMode = "";
    this.modalService.dismissAll()
  }
}
