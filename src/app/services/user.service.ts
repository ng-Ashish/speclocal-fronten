import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AlertService } from './alert.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    user: any;
    msgcredits: number = 0;
    baseUrl: string = environment.baseUrl;
    emailid: any = '';
    sms: boolean;
    template : number;
    constructor(public http: HttpClient, public alertService: AlertService) {

    }

    init() {
        this.user = JSON.parse(localStorage.getItem('currentUser'));
        this.getCredits();
    }

    /**
     * function to get credit 
     */
    getCredits() {
        if (this.user.data.role == "emp") {
            this.emailid = this.user.data.adminemail;
        } else {
            this.emailid = this.user.data.email;
        }
        this.http.get(this.baseUrl + '/user/user/' + this.emailid).subscribe(res => {
            if (res['success']) {
                this.msgcredits = res['data']['msgcredit'];
            }
        });
    }
    /**
     * 
     * @param email Function to get days left of user
     */
    daysleft(email) {
        return this.http.get(this.baseUrl + '/invitation/getdaysleft/' + email);
    }
    /**
    * getuser() : for getting user details for following services
    * 1)sms services, 2)no of employee 3)msg credit service
    * 4) platforms
    */
    getuser() {
        return this.http.get(this.baseUrl + '/user/user/' + this.emailid);
    }
    /**
     * 
     * @param data : request data
     * this function is used to make request for employee
     */
    requestEmployees(data) {
        return this.http.post(this.baseUrl + '/employeerequest/request', data);
    }
    /**
     * this service is used to get sms services of perticular user
     */
    getsmsServices() {
        return this.http.get(this.baseUrl + '/user/user/' + this.user.data.email);
    }
    /**
     * this is for getting the no . of employee count of perticular user
     */
    getnoOfEmployees() {
        return this.http.get(this.baseUrl + '/user/user/' + this.user.data.email);
    }
    /**
     * this function gives the status of employee request i.e request is pending or not
     */
    getStatusOfEmployeeRequest() {
        return this.http.get(this.baseUrl + '/employeerequest/getrequest/' + this.user.data.email)
    }
    /**
     * to get employee count
     */
    getEmployeeCount() {
        return this.http.get(this.baseUrl + '/user/getadminempCount/' + this.user.data.email);
    }
    /**
     * to get status of sms request
     */
    getStatusOfSmsRequest() {
        return this.http.get(this.baseUrl + '/smsservice/getsmsservice/' + this.user.data.email)
    }
    /**
     * get status of msg credit
     */
    getStatusMsgCreditRequest() {
        return this.http.get(this.baseUrl + '/msgCredit/getOnemsgCredit/' + this.user.data.email)
    }
    /**
    * function for grtting platform details like logo url etc
    */
    getplatformDetailsByName(platformname) {
        return this.http.get(this.baseUrl + '/platform/getplatformbyplatformname/' + platformname)
    }
    /**
     * getallavailableplatforms : get all available platforms for request 
     */
    getallavailableplatforms() {
        return this.http.get(this.baseUrl + '/platform/getplatform')
    }
    /**
     * requestplatform : request for platform 
     */
    requestplatform(data) {
        return this.http.post(this.baseUrl + '/platformrequest/request', data)
    }
    /**
     * getStatusOfPlatformRequest : get status of platform request
     */
    getStatusOfPlatformRequest() {
        return this.http.get(this.baseUrl + '/platformrequest/getrequest/' + this.user.data.email)
    }
    /**
     * To get platform with all details about platform
     */
    getplatformwithalldetails() {
        return this.http.get(this.baseUrl + '/user/getuserplatform/' + this.user.data.email)
    }
    /**
     * 
     * @param data : platform data
     * Function is used for delete platform
     */
    deleteplatform(data: any) {
        return this.http.put(this.baseUrl + '/user/deletePlatformByPlatformname', data)
    }
    /**
     * 
     * @param data :holds the platform data
     * function is used for get reviews by filter
     */
    getreviewbyfilter(data : any){
        return this.http.post(this.baseUrl + "/review/getreviewbyfilter",data)
    }
}