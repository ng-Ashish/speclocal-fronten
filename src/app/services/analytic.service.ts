import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AnalyticService {
  
  baseUrl = environment.baseUrl;
  

  constructor(private _http: HttpClient) { }
  
  googleAnalytic(username: string,year : number) {
    var obj = { "email": username,year : year };    
    return this._http.post(this.baseUrl + '/feedback/getreviewbyyearmonth', obj);
  }
  googleAnalyticBySevenDays(username: string,days : number){
    var obj = { "email": username,days : days };    
    return this._http.post(this.baseUrl + '/feedback/getlastsevendayreviews/', obj);
  }
  googleAnalyticByFifteenDays(username: string,days : number){
    var obj = { "email": username,days : days };    
    return this._http.post(this.baseUrl + '/feedback/getlast15dayreviews', obj);
  }
  public getanalyticdatabydays(email : string ,days : number){    
    return this._http.get(this.baseUrl +"/feedback/getanalyticsdatabyday/"+email+"/"+days);
  }
}
