import { Injectable, NgZone } from '@angular/core';
import { GoogleAuthService, GoogleApiService } from "ng-gapi";
import GoogleUser = gapi.auth2.GoogleUser;
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AlertService } from './alert.service';
import { environment } from 'src/environments/environment';
import { SharedService } from '../services/shared.service';


//old
const CLIENT_ID = "442668123902-cflppjlfhghirrfpp9h5ogo79bqqkgd3.apps.googleusercontent.com";
const CLIENT_SECRET = "DXENUUX-m3nDnJpRG-QCZ7sx";

// const CLIENT_ID = "881709144643-3utdrbj18keecnthvc8dpuu613lmii6k.apps.googleusercontent.com";
// const CLIENT_SECRET = "QDu3nWjqbmwqOP8lTvr6Mj8O"

// const CLIENT_ID = "881709144643-3utdrbj18keecnthvc8dpuu613lmii6k.apps.googleusercontent.com";
// const CLIENT_SECRET = "QDu3nWjqbmwqOP8lTvr6Mj8O";

@Injectable({
  providedIn: 'root'
})
export class GoogleService {

  baseUrl: string = environment.baseUrl;
  public static readonly SESSION_STORAGE_KEY: string = "accessToken";
  public static refresh_token: any;
  public user: GoogleUser = undefined;

  isSignedIn: boolean = false;
  refreshToken: any = "";
  userGoogleAccounts: any;
  userGoogleAccountLocations: any;
  userLocationReviews: any = [];
  totalReviewCount: any = 0;
  averageRating: any = 0;
  linkedGoogleAccount: any;
  linkedGoogleAccountLocation: any;
  recentReviews: any = [];

  logged_in_user: any;
  reviews: any = [];
  accounts: any = [];
  locations: any = [];
  refresh_token: any = "";
  linked_account: any;
  linked_location: any;
  average_rating: number = 0;
  total_review_count: number = 0;
  connectedWithGoogle: boolean = false;
  google_data: any;
  nextPageToken: string = "";
  re: any;

  // Insights variable...
  basicInsights: any;
  drivingDirectionsInsights: any;
  basic: any[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  driving: any[] = [];
  totalSearches: number = 0;
  totalViews: number = 0;
  totalActions: number = 0;
  totalActionsLocalPost: number = 0;

  tempReviews: any = [];

  constructor(
    public googleAuthService: GoogleAuthService,
    public googleApiService: GoogleApiService,
    public ngZone: NgZone,
    public _http: HttpClient,
    public alertService: AlertService,
    public sharedService: SharedService
  ) {
    // First make sure gapi is loaded can be in AppInitilizer
    this.googleApiService.onLoad().subscribe();
  }

  init() {
    this.tempReviews = [];
    this.logged_in_user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.logged_in_user.data.role == 'emp') {
      this._http.get(this.baseUrl + '/google/get/' + this.logged_in_user.data.adminemail)
        .subscribe(res => {
          //console.log(res);
          this.google_data = res;
          if (this.google_data.connected_with_google) {
            this.accounts = this.google_data.data.accounts;
            this.locations = this.google_data.data.locations;
            this.linked_account = this.google_data.data.linked_account;
            this.linked_location = this.google_data.data.linked_location;
            this.total_review_count = this.google_data.data.total_review_count;
            this.average_rating = this.google_data.data.average_rating;
            this.refresh_token = this.google_data.data.refresh_token;
            this.reviews = this.google_data.data.reviews;
            this.connectedWithGoogle = this.google_data.connected_with_google;
            this.refreshAccessToken();
            this.recentReviews = this.reviews.slice(0, 2);
            setTimeout(() => {
              this.getInsights();
            }, 3000);
          } else {
            this.connectedWithGoogle = this.google_data.connected_with_google;
            this.reviews = [];
            this.accounts = [];
            this.locations = [];
            this.linked_account = undefined;
            this.linked_location = undefined;
            this.total_review_count = 0;
            this.average_rating = 0;
            this.refresh_token = "";
            this.recentReviews = [];
          }
        });
    } else {
      this._http.get(this.baseUrl + '/google/get/' + this.logged_in_user.data.email)
        .subscribe(res => {
          // //console.log(res);
          this.google_data = res;
          if (this.google_data.connected_with_google) {
            this.accounts = this.google_data.data.accounts;
            this.locations = this.google_data.data.locations;
            this.linked_account = this.google_data.data.linked_account;
            this.linked_location = this.google_data.data.linked_location;
            this.total_review_count = this.google_data.data.total_review_count;
            this.average_rating = this.google_data.data.average_rating;
            this.refresh_token = this.google_data.data.refresh_token;
            this.reviews = this.google_data.data.reviews;
            this.connectedWithGoogle = this.google_data.connected_with_google;
            this.refreshAccessToken();
            this.recentReviews = this.reviews.slice(0, 2);
            setTimeout(() => {
              this.getInsights();
            }, 3000);

          } else {
            this.connectedWithGoogle = this.google_data.connected_with_google;
            this.reviews = [];
            this.accounts = [];
            this.locations = [];
            this.linked_account = undefined;
            this.linked_location = undefined;
            this.total_review_count = 0;
            this.average_rating = 0;
            this.refresh_token = "";
            this.recentReviews = [];
          }
        });
    }
  }

  public getToken(): string {
    let token: string = sessionStorage.getItem(GoogleService.SESSION_STORAGE_KEY);
    if (!token) {
      throw new Error("No token set , authentication required");
    }
    //console.log('token ', token);
    return sessionStorage.getItem(GoogleService.SESSION_STORAGE_KEY);
  }

  public signIn() {
    this.googleAuthService.getAuth().subscribe((auth) => {
      //Normal sign in flow without offline access
      auth.signIn().then(res => this.signInSuccessHandler(res), err => this.signInErrorHandler(err));
    });
  }

  public signInWithOfflineAccess(): void {
    this.googleAuthService.getAuth()
      .subscribe((auth) => {
        // Redirect the user to Google's OAuth 2.0 server to initiate the authentication and authorization process
        auth.grantOfflineAccess({ scope: "profile email https://www.googleapis.com/auth/plus.business.manage", prompt: "consent" })
          .then(res => {
            /**
               * If the user approves the access request, then the response contains an authorization code. 
               * If the user does not approve the request, the response contains an error message.
               * Exchange authorization code for refresh and access tokens
               */
            // //console.log('business api >>', res)
            /*
            Use redirect url : http://localhost:4200 when on localhost
            otherwise use https://speclocal.com
            */
            this._http.post('https://www.googleapis.com/oauth2/v4/token', {
              code: res['code'],
              client_id: CLIENT_ID,
              client_secret: CLIENT_SECRET,
              redirect_uri: "https://speclocal.com",
              // redirect_uri: "http://localhost:4200", // local
              grant_type: "authorization_code"
            }).subscribe(res => {
              // //console.log('res  >', res);
              this.grantOfflineAccessSuccessHandler(res);
            });
          }).catch(this.signInErrorHandler);
      });
  }

  public grantOfflineAccessSuccessHandler(res: any) {
    // Store refresh_token on long term storage
    this.refreshToken = res.refresh_token;
    // Store the access_token in sessionStorage
    // //console.log('res.refresh_token >>>', res.refresh_token);
    sessionStorage.setItem(GoogleService.SESSION_STORAGE_KEY, res.access_token);
    this.isSignedIn = true;
    this.getGoogleAccounts();
  }

  public signInSuccessHandler(res: GoogleUser) {
    // //console.log(res);
    this.ngZone.run(() => {
      this.user = res;
      sessionStorage.setItem(
        GoogleService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token
      );
      this.isSignedIn = true;
      this.getGoogleAccounts();
    });
  }

  public signInErrorHandler(err: any) {
    console.warn(err);
    this.isSignedIn = false;
  }

  async refreshAccessToken() {
    await this._http.post('https://www.googleapis.com/oauth2/v4/token', {
      refresh_token: this.getRefreshToken(),
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: "refresh_token"
    }).toPromise().then((res: any) => {
      // //console.log(res);
      sessionStorage.setItem(GoogleService.SESSION_STORAGE_KEY, res.access_token);
    });
  }

  public getRefreshToken() {
    return this.refresh_token;
  }

  public getGoogleAccounts() {
    this._http.get('https://mybusiness.googleapis.com/v4/accounts?access_token=' + this.getToken())
      .subscribe(res => {
        // //console.log(res);
        this.userGoogleAccounts = res['accounts'];
      });
  }

  public getGoogleAccountLocations(account: any) {
    this.linkedGoogleAccount = account;
    this._http.get('https://mybusiness.googleapis.com/v4/' + account.name + '/locations?access_token=' + this.getToken())
      .subscribe(res => {
        // //console.log(res);
        this.userGoogleAccountLocations = res['locations'];
      });
  }

  public postReply(reviewName: string, data: any) {
    this._http.put('https://mybusiness.googleapis.com/v4/' + reviewName + '/reply?access_token=' + this.getToken(), data).subscribe(res => {
      // //console.log(res);
    });
  }

  public quickPost(locationName: string, req: any) {
    this._http.post('https://mybusiness.googleapis.com/v4/' + locationName + '/localPosts?access_token=' + this.getToken(), req)
      .subscribe(res => {
        // //console.log(res);
      });
  }

  public getLocationReviews(location: any) {
    this.linkedGoogleAccountLocation = location;
    // //console.log(location)
    this._http.get('https://mybusiness.googleapis.com/v4/' + location.name + '/reviews?access_token=' + this.getToken() + '&pageSize=200')
      .subscribe(res => {
        // //console.log(res);
        this.userLocationReviews = res['reviews'];
        this.averageRating = res['averageRating'];
        this.totalReviewCount = res['totalReviewCount'];
        this.savelinkedAccountInfo();
      });
  }



  public savelinkedAccountInfo() {
    let data = {
      email: this.logged_in_user.data.email,
      connected_with_google: true,
      accounts: this.userGoogleAccounts,
      locations: this.userGoogleAccountLocations,
      linked_account: this.linkedGoogleAccount,
      linked_location: this.linkedGoogleAccountLocation,
      total_review_count: this.totalReviewCount,
      average_rating: this.averageRating,
      reviews: this.userLocationReviews,
      // reviews: this.tempReviews,
      refresh_token: this.refreshToken
    }

    this._http.post(this.baseUrl + '/google/post', data).subscribe(res => {
      // //console.log("MY RESPOCE : " , res);
      this.sharedService.syncedPlatform.push('google');
      this.init();
      this.alertService.success('Connected With Google Successfully!', false);
      this.totalReviewCount = 0;
      this.averageRating = 0;
      this.tempReviews = [];
    }, error => {
      // this.alertService.error('Error!', false);
    });
  }

  public syncReviews() {
    // //console.log('Syncing google reviews...');
    if (this.reviews.length < this.tempReviews.length) {
      let data = {
        email: this.logged_in_user.data.email,
        reviews: this.tempReviews.slice(0, (this.tempReviews.length - this.reviews.length) + 1),
        total_review_count: this.totalReviewCount,
        average_rating: this.averageRating
      };      
    }
  }

  public getGoogleReview(userName: string) {
    return this._http.get(this.baseUrl + "/" + environment.googlereview + "/" + userName);
  }
  public getLastSevenDaysGoogleReview(userName: string) {
    return this._http.get(this.baseUrl + "/" + environment.lastsevendaysgooglereview + "/" + userName)
  }
  public getLastFifteenDaysGoogleReview(userName: string) {
    return this._http.get(this.baseUrl + "/" + environment.lastfifteendaysgooglereview + "/" + userName)
  }
  public getInsights() {

    let current_date = new Date();
    let curr = current_date.toISOString().toString();

    let prev_date = new Date();
    prev_date.setMonth(prev_date.getMonth() - 1);
    let prev = prev_date.toISOString().toString();

    this._http.post('https://mybusiness.googleapis.com/v4/' + this.linked_account.name + '/locations:reportInsights?access_token=' + this.getToken(),
      {
        "locationNames": [
          this.linked_location.name
        ],
        "basicRequest": {
          "metricRequests": [
            {
              "metric": "ALL",
              "options": [
                "AGGREGATED_TOTAL"
              ]
            }
          ],
          "timeRange": {
            "startTime": prev,
            "endTime": curr
          }
        },
      }).subscribe(res => {
        // //console.log(res);
        this.basicInsights = res;
        this.basic[0] = +this.basicInsights.locationMetrics[0].metricValues[0].totalValue.value;
        this.basic[1] = +this.basicInsights.locationMetrics[0].metricValues[1].totalValue.value;
        this.basic[2] = +this.basicInsights.locationMetrics[0].metricValues[2].totalValue.value;
        this.basic[3] = +this.basicInsights.locationMetrics[0].metricValues[3].totalValue.value;
        this.basic[4] = +this.basicInsights.locationMetrics[0].metricValues[4].totalValue.value;
        this.basic[5] = +this.basicInsights.locationMetrics[0].metricValues[5].totalValue.value;
        this.basic[6] = +this.basicInsights.locationMetrics[0].metricValues[6].totalValue.value;
        this.basic[7] = +this.basicInsights.locationMetrics[0].metricValues[7].totalValue.value;
        this.basic[8] = +this.basicInsights.locationMetrics[0].metricValues[8].totalValue.value;
        this.basic[9] = +this.basicInsights.locationMetrics[0].metricValues[9].totalValue.value;
        this.basic[10] = +this.basicInsights.locationMetrics[0].metricValues[10].totalValue.value;
        this.basic[11] = +this.basicInsights.locationMetrics[0].metricValues[11].totalValue.value;
        this.basic[12] = +this.basicInsights.locationMetrics[0].metricValues[12].totalValue.value;
        this.basic[13] = +this.basicInsights.locationMetrics[0].metricValues[13].totalValue.value;
        this.totalSearches = this.basic[0] + this.basic[1] + this.basic[2];
        this.totalViews = this.basic[3] + this.basic[4];
        this.totalActions = this.basic[5] + this.basic[6] + this.basic[7];
        this.totalActionsLocalPost = this.basic[12] + this.basic[13];
        let emailid;
        if(this.logged_in_user.data.role =='emp'){
          emailid = this.logged_in_user.data.adminemail;
        }else{
          emailid = this.logged_in_user.data.email;
        }
        // console.log("email : ", emailid)
        let data ={

          email : emailid,
          platform : "google",
          insight :{
            "Direct" : this.basic[0],
            "Discovery" : this.basic[1],
            "Branded" : this.basic[2],
            "Listing On Search" : this.basic[3],
            "Listing On Maps" : this.basic[4],
            "Visit Your Website" : this.basic[5],
            "Call You" : this.basic[6],
            "Request Directions" : this.basic[7],
            "Local Post Viewed" : this.basic[12],
            "Call-To-Action" : this.basic[13] ,
            "Total Actions Local Post": this.totalActionsLocalPost,
            "Total Views":this.totalViews,
            "Total Searches":this.totalSearches,   
            "Total Actions":this.totalActions   
           }
        }
        
         this._http.post(this.baseUrl + '/insight/postinsightdata' ,data).subscribe(res=>{
          console.log("res : " , res)
         })
      });

    this._http.post('https://mybusiness.googleapis.com/v4/' + this.linked_account.name + '/locations:reportInsights?access_token=' + this.getToken(),
      {
        "locationNames": [
          this.linked_location.name
        ],
        "drivingDirectionsRequest": {
          "numDays": 'THIRTY',
        }
      }).subscribe(res => {
        // //console.log(res);
        this.drivingDirectionsInsights = res;
        if (this.drivingDirectionsInsights.locationDrivingDirectionMetrics[0].topDirectionSources) {
          this.driving = this.drivingDirectionsInsights.locationDrivingDirectionMetrics[0].topDirectionSources[0].regionCounts;
        }
      }, error => {
        // this.alertService.error('Error', false);
      });

  }
  userData: any = {};
  public getUserData(email) {
    this._http.get(this.baseUrl + '/google/get/' + email)
      .subscribe((res: any) => {
        // //console.log(res)
        this.userData = res;
        // console.log('this.userData    ', this.userData)        
        this.refreshAccessToken();
        var linkedaccountlocation = this.userData.data.linked_location.name;
        var location_to_sync = this.userData.data.locations.filter(loc=>{
           return loc.name == linkedaccountlocation
        })
        location_to_sync.forEach(element => {
          this.getLocationReviews2(element.name, "")
        });                                
        // this.manualCronFb(res.data.linked_page.id, res.data.linked_page.access_token, null);
      });
  }
  async getLocationReviews2(location: any, nextPageToken: string) {
    this.linkedGoogleAccountLocation = location;
    if (nextPageToken == "") {
      await this._http.get('https://mybusiness.googleapis.com/v4/' + location + '/reviews?access_token=' + this.getToken() + '&pageSize=10')
        .toPromise().then(res => {
          console.log('first page ', res);
          if (res['reviews'].length > 0) {
            this.nextPageToken = res['nextPageToken'];
            this._http.post(environment.baseUrl + "/google/manualcron", {
              email: this.logged_in_user.data.email,
              reviews: res['reviews']
            }).subscribe((resp: any) => {
              // //console.log(resp)
            });
            this.getLocationReviews2(location, this.nextPageToken);
          } else {

            this.alertService.success('Google Review Sync complete', false); return;
          }
        });
    } else {
      if (nextPageToken == undefined) {
        return;
      } else {
        await this._http.get('https://mybusiness.googleapis.com/v4/' + location + '/reviews?access_token=' + this.getToken() + '&pageSize=10&pageToken=' + nextPageToken)
          .toPromise().then(res => {
            // //console.log('new page  ', res);
            if (res['nextPageToken']) {
              this.nextPageToken = res['nextPageToken'];
              this._http.post(environment.baseUrl + "/google/manualcron", {
                email: this.logged_in_user.data.email,
                reviews: res['reviews']
              }).subscribe((resp: any) => {
                // //console.log(resp)
              });
              this.getLocationReviews2(location, this.nextPageToken);
            } else {
              this.alertService.success('Google Review Sync complete', false);
            }
          });
      }
    }
  }
}
