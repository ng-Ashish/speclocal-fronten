import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from './alert.service';
import { environment } from 'src/environments/environment';
import { SharedService } from '../services/shared.service';

const headers: any = new HttpHeaders({
  'Content-Type': 'application/json',
  'user-key': "0c2e765b2806ea498a273714f8a2d953"
});

@Injectable({
  providedIn: 'root'
})
export class ZomatoService {

  baseUrl: string = environment.baseUrl;
  // restaurantID: any;
  restaurant_info: any;
  restaurant_reviews: any;
  recentReviews: any = [];

  res_id: number = null;
  res_name: string = "";
  reviews: any = [];
  restaurant_data: any;
  average_rating: number;
  connectedWithZomato: boolean = false;
  user: any;

  constructor(public http: HttpClient, public alertService: AlertService,public sharedService : SharedService) { }

  init() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if(this.user.data.role == 'emp') {
      this.http.get(this.baseUrl + '/zomato/get/' + this.user.data.adminemail)
      .subscribe(res => {
        // console.log(res);
        this.restaurant_data = res;
        if (this.restaurant_data.connected_with_zomato) {
          this.res_id = this.restaurant_data.data.res_id;
          this.res_name = this.restaurant_data.data.res_name;
          this.reviews = this.restaurant_data.data.reviews;
          this.connectedWithZomato = this.restaurant_data.connected_with_zomato;
          this.average_rating = this.calculateAverageRating(); 
          this.recentReviews = this.reviews.slice(0,1);
        } else {
          this.res_id = undefined;
          this.res_name = "";
          this.reviews = [];
          this.average_rating = 0;
          this.connectedWithZomato = false;
          this.recentReviews = [];
        }
      })
    } else {
      this.http.get(this.baseUrl + '/zomato/get/' + this.user.data.email)
      .subscribe(res => {
        // console.log(res);
        this.restaurant_data = res;
        if (this.restaurant_data.connected_with_zomato) {
          this.res_id = this.restaurant_data.data.res_id;
          this.res_name = this.restaurant_data.data.res_name;
          this.reviews = this.restaurant_data.data.reviews;
          this.connectedWithZomato = this.restaurant_data.connected_with_zomato;
          this.average_rating = this.calculateAverageRating(); 
          this.recentReviews = this.reviews.slice(0,1);
        } else {
          this.res_id = undefined;
          this.res_name = "";
          this.reviews = [];
          this.average_rating = 0;
          this.connectedWithZomato = false;
          this.recentReviews = [];
        }
      })
    }
  }

  public calculateAverageRating() {
    var rating: number;
    var sum: number = 0;
    this.reviews.forEach(element => {
      sum = sum + element.review.rating;
    });
    rating = sum / this.reviews.length;
    return rating;
  }

  public getRestaurantDetails(data: any) {
    // this.restaurantID = data.restaurant_id;
    this.http.get('https://developers.zomato.com/api/v2.1/restaurant?res_id=' + data.restaurant_id, { headers: headers })
      .subscribe(res => {
        // console.log(res);
        this.restaurant_info = res;
      }, error => {
        this.restaurant_info = undefined;
      });
  }

  public getRestaurantReviews() {
    this.http.get('https://developers.zomato.com/api/v2.1/reviews?res_id=' + this.restaurant_info.id, { headers: headers })
      .subscribe(res => {
        // console.log(res);
        this.restaurant_reviews = res['user_reviews'];
        this.postRestaurantInfo();
      });
  }

  postRestaurantInfo () {
    let data = {
      email: this.user.data.email,
      res_id: this.restaurant_info.id,
      res_name: this.restaurant_info.name,
      reviews: this.restaurant_reviews,
      connected_with_zomato: true
    }
    this.http.post(this.baseUrl + '/zomato/post', data).subscribe(res => {
      this.sharedService.syncedPlatform.push('zomato');
      this.init();
      this.alertService.success('Connected With Zomato Successfully!', false);      
    }, error => {
      // this.alertService.error('Error',false);
    });
  }
  public getZomatoReview(userName: string) {
    return this.http.get(this.baseUrl+"/"+environment.zomatoreview + "/" + userName);
  }
}
