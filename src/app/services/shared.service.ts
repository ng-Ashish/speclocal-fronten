import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  syncedPlatform = [];
  isFeedbakTemplate : boolean;
  constructor(public _http : HttpClient) {
    this.isFeedbakTemplate = false;
   }
  getOneFeedbackForm(email : string){
    this._http.get(environment.baseUrl + '/feedbackform/getOneFeedbackForm/' + email).subscribe((res:any)=>{
      console.log(res)
      if(res.success){
        this.isFeedbakTemplate = true;
      }else{
        this.isFeedbakTemplate = false;
      }
    })
  }
}
