import { Injectable } from '@angular/core';
import { AlertService } from './alert.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  baseUrl: string = environment.baseUrl;

  constructor(public alertService: AlertService, public http: HttpClient) { }

  login(email: string, password: string) {
    
    return this.http.post<any>(this.baseUrl + '/auth/login', { email: email, password: password })
      .pipe(map(user => {
        // console.log(user);
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;
      }));
  }

  resetPassword(email : any){
    return this.http.post(this.baseUrl + '/auth/forgetpassword', { email: email })
  }
  
  logout() {
    // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
  }

}
