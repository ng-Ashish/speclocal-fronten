import { Injectable } from '@angular/core';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { HttpClient } from '@angular/common/http';
import { AlertService } from './alert.service';
import { environment } from 'src/environments/environment';
import { SharedService } from '../services/shared.service';

// Facebook APP_ID and APP-SECRET
const APP_ID = '2275602422705034';
const APP_SECRET = 'b61e523435522f981742d19d49e54e36';

@Injectable({
  providedIn: 'root'
})
export class FbService {

  photoId: number;
  handleError: any;
  shortLivedAccessToken: any;
  longLiveAccessToken: any;
  accountInfo: any;
  pageAccessTokens: any;
  facebookPageReviews: any;
  linkedFacebookPage: any;
  recentReviews: any = [];

  user: any;
  account: any;
  reviews: any = [];
  linked_page: any;
  facebook_data: any;
  page_access_tokens: any = [];
  average_rating: number = 0;
  connectedWithFacebook: boolean = false;

  baseUrl: string = environment.baseUrl;
  tempReviews: any[] = [];

  constructor(public fb: FacebookService, public http: HttpClient, public alertService: AlertService, public sharedService: SharedService) {
    //Initialization of Facebook SDK
    // let initParams: InitParams = {
    //   appId: APP_ID,
    //   xfbml: true,
    //   version: 'v3.2'
    // };
    let initParams: InitParams = {
      appId: APP_ID,
      cookie: true,
      status: true,
      xfbml: true,
      version: 'v3.2'
    };
    fb.init(initParams);
  }

  init() {
    this.tempReviews = [];
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == 'emp') {
      this.http.get(this.baseUrl + '/facebook/get/' + this.user.data.adminemail)
        .subscribe(res => {
          console.log(res);
          this.facebook_data = res;
          if (this.facebook_data.connected_with_facebook) {
            this.account = this.facebook_data.data.account;
            this.reviews = this.facebook_data.data.reviews;
            this.linked_page = this.facebook_data.data.linked_page;
            this.page_access_tokens = this.facebook_data.data.page_access_tokens;
            this.connectedWithFacebook = this.facebook_data.connected_with_facebook;
            if (this.reviews.length != 0) {
              this.average_rating = this.calculateAverageRating();
            }
            this.recentReviews = this.reviews.slice(0, 2);
          } else {
            this.account = [];
            this.reviews = [];
            this.linked_page = undefined;
            this.page_access_tokens = [];
            this.average_rating = 0;
            this.connectedWithFacebook = this.facebook_data.connected_with_facebook;
            this.recentReviews = [];
          }
        });
    } else {
      this.http.get(this.baseUrl + '/facebook/get/' + this.user.data.email)
        .subscribe(res => {
          // console.log(res);
          this.facebook_data = res;
          if (this.facebook_data.connected_with_facebook) {
            this.account = this.facebook_data.data.account;
            this.reviews = this.facebook_data.data.reviews;
            this.linked_page = this.facebook_data.data.linked_page;
            this.page_access_tokens = this.facebook_data.data.page_access_tokens;
            this.connectedWithFacebook = this.facebook_data.connected_with_facebook;
            if (this.reviews.length != 0) {
              this.average_rating = this.calculateAverageRating();
            }
            this.recentReviews = this.reviews.slice(0, 2);
          } else {
            this.account = [];
            this.reviews = [];
            this.linked_page = undefined;
            this.page_access_tokens = [];
            this.average_rating = 0;
            this.connectedWithFacebook = this.facebook_data.connected_with_facebook;
            this.recentReviews = [];
          }
        });
    }    
  }

  public calculateAverageRating() {
    var rating: number;
    var sum: number = 0;
    this.reviews.forEach(element => {
      sum = sum + this.getStarRatingValue(element.recommendation_type);
    });
    rating = sum / this.reviews.length;
    return rating;
  }

  public getStarRatingValue(starRating: string): number {
    if (starRating == "positive") {
      return 5;
    }
    return 0;
  }

  /**
   * Login with minimal permissions. This allows you to see their public profile only.
   */
  login() {
    this.fb.login()
      .then((res: LoginResponse) => {
        // console.log('Logged in', res);
      })
      .catch(this.handleError);
  }

  /**
   * Login with additional permissions/options
   */
  loginWithOptions() {
    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: 'public_profile,email,pages_show_list,manage_pages,publish_pages,read_insights'
    };

    this.fb.login(loginOptions)
      .then((res: LoginResponse) => {
        this.shortLivedAccessToken = res; // 2                
        this.getLongLivedAccessToken();
      })
      .catch(this.handleError);
  }

  /**
   * Get Login status
   */
  getLoginStatus() {
    this.fb.getLoginStatus()
      .then(console.log.bind(console))
      .catch(console.error.bind(console));
  }

  /**
   * Get Long Live Access Token from Short Live Access Token
   */
  getLongLivedAccessToken() {
    this.fb.api('https://graph.facebook.com/v3.2/oauth/access_token?grant_type=fb_exchange_token&client_id=' + APP_ID + '&client_secret=' + APP_SECRET + '&fb_exchange_token=' + this.shortLivedAccessToken.authResponse.accessToken, 'get')
      .then((res: any) => {
        // 3
        this.longLiveAccessToken = res;
        this.getAccountID();
      })
      .catch(this.handleError);
  }

  /**
   * Get Account ID form Long Live Access Token
   */
  getAccountID() {
    this.fb.api('https://graph.facebook.com/v3.2/me?access_token=' + this.longLiveAccessToken.access_token, 'get')
      .then((res: any) => {
        this.accountInfo = res; // 4      
        this.getPageAccessToken();
      })
      .catch(this.handleError);
  }

  /**
   * Get Facebook Page Access Token
   */
  getPageAccessToken() {
    this.fb.api('https://graph.facebook.com/v3.2/' + this.accountInfo.id + '/accounts?access_token=' + this.longLiveAccessToken.access_token, 'get')
      .then((res: any) => {
        this.pageAccessTokens = res;
        // console.log('this.pageAccessTokens >>>', this.pageAccessTokens)
      })
      .catch(this.handleError);
  }

  /**
   * Get Facebook Page ratings and reviews
   */
  getPageReviews(linkedFacebookPage: any) {
    this.linkedFacebookPage = linkedFacebookPage;
    this.fb.api('https://graph.facebook.com/v3.2/' + linkedFacebookPage.id + '/ratings?fields=has_review,has_rating,created_time,recommendation_type,open_graph_story,rating,review_text,reviewer&access_token=' + linkedFacebookPage.access_token, 'get')
      .then((res: any) => {
        this.facebookPageReviews = res.data;
        this.savelinkedAccountInfo();
        // console.log(this.facebookPageReviews)
      })
      .catch(this.handleError);
  }

  public savelinkedAccountInfo() {
    let data = {
      email: this.user.data.email,
      connected_with_facebook: true,
      account: this.accountInfo,
      page_access_tokens: this.pageAccessTokens.data,
      linked_page: this.linkedFacebookPage,
      reviews: this.facebookPageReviews
      // reviews: this.tempReviews
    };
    this.http.post(this.baseUrl + '/facebook/post', data).subscribe(res => {
      this.sharedService.syncedPlatform.push('fb');
      this.init();
      this.alertService.success('Connected With Facebook Successfully!', false);
      this.tempReviews = [];
    }, error => {
      // this.alertService.error('Error', false);
    });
  }


  quickPost(linkedFacebookPage: any, req: any) {
    if (req.source == "" && req.link == "") {
      // console.log('Post with message only...');
      this.fb.api(
        'https://graph.facebook.com/' + linkedFacebookPage.id + '/feed?access_token=' + linkedFacebookPage.access_token,
        'post', { message: req.message })
        .then(res => {
          // console.log(res);
        }).catch(this.handleError);
    }

    if (req.source != "" && req.link == "") {
      // console.log('Post with message & image... ');
      this.fb.api('https://graph.facebook.com/' + linkedFacebookPage.id + '/photos?access_token=' + linkedFacebookPage.access_token,
        'post', { url: req.source, caption: req.message, published: true }).then(res => {
          // console.log(res);
        });
    }

    if (req.source != "" && req.link != "") {
      // console.log('Post with message, image & link... ');
      this.fb.api(
        'https://graph.facebook.com/' + linkedFacebookPage.id + '/feed?access_token=' + linkedFacebookPage.access_token,
        'post', { message: req.message, source: req.source, link: req.link })
        .then(res => {
          // console.log(res);
        }).catch(this.handleError);
    }

  }

  quickPostWithMultipleImages(linkedFacebookPage: any, req: any) {

    if (req.images.length == 0) {
      // console.log('Post with message only...');
      this.fb.api(
        'https://graph.facebook.com/' + linkedFacebookPage.id + '/feed?access_token=' + linkedFacebookPage.access_token,
        'post', { message: req.message })
        .then(res => {
          // console.log(res);
        }).catch(this.handleError);
    }

    if (req.images.length != 0) {
      // console.log('Post with message & image... ');
      let attached_media: any = [];
      req.images.forEach(item => {
        this.fb.api('https://graph.facebook.com/' + linkedFacebookPage.id + '/photos?access_token=' + linkedFacebookPage.access_token,
          'post', { url: item, caption: req.message, published: false }).then(res => {
            // console.log(res);
            attached_media.push({
              "media_fbid": res.id
            });
          });
      });
      setTimeout(() => {
        // console.log(attached_media);
        this.fb.api('https://graph.facebook.com/' + linkedFacebookPage.id + '/feed?access_token=' + linkedFacebookPage.access_token,
          'post', { "message": req.message, "attached_media": attached_media }).then(result => {
            // console.log(result);
          });
      }, 10000);

    }
  }

  /*
    get facebook review
  */
  public getFaceBookReview(userName: string) {
    return this.http.get(this.baseUrl + "/" + environment.facebookreview + "/" + userName);
  }
  
  public getLastSevenDaysFaceBookReview(userName: string) {
    return this.http.get(this.baseUrl + '/' + environment.lastsevendaysfacebookreview + "/" + userName)
  }
  /*
    get last 15 days facebook review
  */
  public getLastFifteenDaysFaceBookReview(userName: string) {
    return this.http.get(this.baseUrl + '/' + environment.lastfifteendaysfacebookreview + "/" + userName)
  }

  userData: any = {};
  public getUserData(email) {
    // let email = 
    this.http.get(this.baseUrl + '/facebook/get/' + email)
      .subscribe((res: any) => {
        // console.log(res)
        this.userData = res;
        this.manualCronFb(res.data.linked_page.id, res.data.linked_page.access_token, null);
      });
  }

  manualCronFb(pageid: any, accesstoken: any, nextpageUrl?: any) {
    if (nextpageUrl == null) {
      this.fb.api('https://graph.facebook.com/v3.2/' + pageid + '/ratings?fields=has_review,has_rating,created_time,recommendation_type,open_graph_story,rating,review_text,reviewer&access_token=' + accesstoken + '&limit=2', 'get')
        .then((res: any) => {
          // console.log(res)
          if (res.paging == undefined) { // no page for next link in first hit
            // console.log('fetch comlete');
          } else {
            this.http.post(environment.baseUrl + "/" + environment.manualcron, {
              email: this.user.data.email,
              review: res.data
            }).subscribe(resp => {

            })
            var nextPage = res.paging.next;
            this.manualCronFb(pageid, accesstoken, nextPage);
          }
        })
    } else {
      var nextUrl = nextpageUrl + `&access_token=${accesstoken}`
      this.fb.api(nextUrl + '&limit=2', 'get')
        .then((res: any) => {
          // console.log(res)
          if (res.paging == undefined) { // no page for next link in first hit
            // console.log('fetch comlete');
          } else {
            this.http.post(environment.baseUrl + "/" + environment.manualcron, {
              email: this.user.data.email,
              review: res.data
            }).subscribe(resp => {

            })
            var nextPage = res.paging.next;
            this.manualCronFb(pageid, accesstoken, nextPage);
          }
        })
    }
    this.alertService.success('Facebook New Review Synced', false);
  }

  getfbinsight(email){
    return this.http.get(this.baseUrl + '/facebook/get/' + email);
  }  
}
