import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class FeedbackformserviceService {

  baseUrl: string = environment.baseUrl;
  constructor(public _http : HttpClient) { }


  addFeedbackForm(data : any){
    return this._http.post(this.baseUrl + '/feedbackform/addfeedbackform' , data)
  }

  getFeedbackForm(){
    return this._http.get(this.baseUrl + '/feedbackform/addfeedbackform')
  }
  updateFeedbackForm(udata : any){
    return this._http.put(this.baseUrl + '/feedbackform/updatefeedbackform' , udata)
  }
  deleteOneFeedbackForm(id : any){
    return this._http.delete(this.baseUrl + '/feedbackform/deleteonefeedbackform/' + id)
  }
  getOneFeedbackForm(email : string){
    return this._http.get(this.baseUrl + '/feedbackform/getOneFeedbackForm/' + email)
  }
}
