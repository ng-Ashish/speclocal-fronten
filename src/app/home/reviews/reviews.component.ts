import { Component, OnInit } from '@angular/core';
import { FbService } from 'src/app/services/fb.service';
import { GoogleService } from 'src/app/services/google.service';
import { ZomatoService } from 'src/app/services/zomato.service';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

import { getLocaleExtraDayPeriods, DatePipe } from '@angular/common';
import { AnalyticService } from 'src/app/services/analytic.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { HttpClient } from '@angular/common/http';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  baseUrl: string = environment.baseUrl;
  googleReplyForm: FormGroup;
  reviewFor: any = {};
  selPlatform = '';
  selrate = '';
  selDate = '';
  user: any = {};
  filterreviews: any = [];

  mainArray: any
  tempArray: any

  inprocess: boolean = true;
  constructor(
    public fbService: FbService,
    public googleService: GoogleService,
    public zomatoService: ZomatoService,
    config: NgbRatingConfig,
    public alertService: AlertService,    
    public analyticService: AnalyticService,
    public userservice: UserService,
    public http: HttpClient,
    public datepipe: DatePipe,
    public sharedService : SharedService
  ) {
    config.max = 5;
    config.readonly = true;
    this.selPlatform = '';
    this.selrate = '';
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == 'emp') {
      this.reviewFor = this.user.data.adminemail
    } else {
      this.reviewFor = this.user.data.email
    }
    this.getusersSmsservicestatus();
    var defaultfilter = {
      platform: '',
      rate: '',
      email: this.reviewFor,
      date: ''
    };
    this.reload(defaultfilter);
    
    this.sharedService.getOneFeedbackForm(this.reviewFor);
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }


  returnDate(date) {
    if (date == " ") {
      return "";
    } else {
      return this.datepipe.transform(date, 'yyyy-MM-dd');
    }
  }
  ngOnInit() {
    this.googleReplyForm = new FormGroup({
      comment: new FormControl('', Validators.required)
    });
  }
  //dont touch
  //posting  reply on google review
  postGoogleReply(review_name: string) {
    if (this.googleReplyForm.invalid) {
      this.alertService.warning('Comment required!', false);
      return;
    }
    this.googleService.postReply(review_name, this.googleReplyForm.value);
    this.googleReplyForm.reset({ comment: "" });
    this.alertService.success('Reply sent successfully!', false);
  }
/**
 * 
 * @param filter 
 * realoding the reviews
 */
  reload(filter?: any) {
    this.inprocess = true;
    if (filter != undefined) {
      this.userservice.getreviewbyfilter(filter).subscribe((res: any) => {
        if (res.data.length > 0) {
          this.inprocess = false;
          this.filterreviews = res.data;
          this.mainArray = res.data;
          this.tempArray = res.data.slice(0, 20)
        } else {
          this.filterreviews = [];
          this.mainArray = [];
          this.tempArray = [];
        }
      });
    } else {
      var defaultfilter = {
        platform: this.selPlatform,
        rate: this.selrate,
        email: this.reviewFor,
        date: this.selDate
      };
      this.userservice.getreviewbyfilter(defaultfilter).subscribe((res: any) => {
        if (res.data.length > 0) {
          this.inprocess = false;
          this.filterreviews = res.data;
          this.mainArray = res.data
          this.tempArray = res.data.slice(0, 20)
        } else {
          this.filterreviews = [];
          this.mainArray = [];
          this.tempArray = [];
        }
      });
    }
  }

  onScrollDown() {
    if (this.tempArray.length < this.mainArray.length) {
      let len = this.tempArray.length;
      for (let i = len; i <= len + 20; i++) {
        this.tempArray.push(this.mainArray[i]);
      }
    }
  }
  convertUnixToDate(date: any) {
    return new Date(date * 1000);
  }
  sync() {
    this.fbService.getUserData(this.reviewFor);
    this.googleService.getUserData(this.reviewFor);
  }
  getusersSmsservicestatus() {
    let emailid;
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // ////console.log("res  : ", res);
      if (res["success"]) {
        this.userservice.sms = res['data'].smscampaining;
        ////console.log("smscampaining : ", this.userservice.sms)
      }
    })
  }
  selectPlatform(platform?: any) {
    this.selPlatform = platform.target.value;
  }
  selectByRating(rating?: any) {
    this.selrate = rating.target.value;
  }
  selectByDate(date?: any) {
    this.selDate = date.target.value
  }
  //dont touch
}
