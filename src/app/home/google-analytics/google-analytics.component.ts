import { Component, OnInit } from '@angular/core';
import { ZomatoService } from 'src/app/services/zomato.service';
import { GoogleService } from 'src/app/services/google.service';
import { FbService } from 'src/app/services/fb.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

@Component({
  selector: 'app-google-analytics',
  templateUrl: './google-analytics.component.html',
  styleUrls: ['./google-analytics.component.css']
})
export class GoogleAnalyticsComponent implements OnInit {

  user: any;
  baseUrl: string = environment.baseUrl;
  constructor(
    public fbService: FbService,
    public googleService: GoogleService,
    public zomatoService: ZomatoService,
    public http: HttpClient,
    public userservice: UserService,
    public sharedService : SharedService
  ) {
    this.getusersSmsservicestatus();
    this.user = JSON.parse(localStorage.getItem('currentUser'));

    var emailid = '';
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }    
    this.sharedService.getOneFeedbackForm(emailid);
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  ngOnInit() {
  }
  getusersSmsservicestatus() {
    let emailid;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        this.userservice.sms = res['data'].smscampaining;
        //console.log("smscampaining : ", this.userservice.sms)
      }
    })
  }

}
