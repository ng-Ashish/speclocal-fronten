import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/services/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FeedbackformserviceService } from 'src/app/services/feedbackformservice.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit, OnDestroy {

  user: any;
  baseUrl: string = environment.baseUrl;
  FEEDBACKS: any = [];
  uniqueFeedbacks: any = [];
  usersFeedback: any = [];
  selectedFeedback: any;
  feedbackForm: FormGroup;

  page = 1;
  pageSize = 10;
  collectionSize = 0;
  emailid: any;
  adminid: any;
  feedbacktemplate: any = {};
  selectedUserdata = {};
  constructor(
    public http: HttpClient,
    public modalService: NgbModal,
    config: NgbRatingConfig,
    public alertService: AlertService,
    public router: Router,
    public feedbacktemplateService: FeedbackformserviceService,
    public sharedService : SharedService) {
    // customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = true;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.invalidUserStatus();

    if (this.user.data.role == "emp") {
      this.emailid = this.user.data.adminemail;
      this.adminid = this.user.data.admin_id;
    } else {
      this.emailid = this.user.data.email;
      this.adminid = this.user.data._id;
    }
    this.feedbackformtemplate()
       
    this.sharedService.getOneFeedbackForm(this.emailid);
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.user.data.email).subscribe(res => {
      // //console.log("res : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.feedbackForm = new FormGroup({
      client_id: new FormControl('', Validators.required),
      customer_id: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
      phoneno: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required)
    })
    this.getFeedback();
  }

  ngOnDestroy() {
    this.FEEDBACKS = [];
  }

  getFeedback() {
    this.http.get(this.baseUrl + '/feedback/get/' + this.adminid)
      .subscribe(res => {
        if (res['success']) {
          this.FEEDBACKS = res['data'];
          let data = res['data'];
          let flag = false;
          for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < this.uniqueFeedbacks.length; j++) {
              if (data[i].customerData.phoneno == this.uniqueFeedbacks[j].customerData.phoneno) {
                flag = true;
                this.uniqueFeedbacks[j] = data[i];
                break;
              }
            }
            if (flag == false) {
              this.uniqueFeedbacks.push(data[i]);
            }
            flag = false;
          }
          this.uniqueFeedbacks.reverse();
          this.collectionSize = this.uniqueFeedbacks.length;
        } else {
          // this.alertService.error('Something went wrong!', false);
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
  }


  get myfeedbacks() {
    return this.uniqueFeedbacks
      .map((feedback, i) => ({ ...feedback }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  feedbackformtemplate() {

    this.feedbacktemplateService.getOneFeedbackForm(this.emailid).subscribe((templateres: any) => {
      // this.feedbacktemplate = template.data

      if (templateres.success) {
        this.feedbacktemplate = templateres.data.feedbackFormData.filter(res => {
          return res.questionText != null
        });
      }
    });

  }
  ansArray = [];
  showDetails(content, feedback: any) {
    this.selectedUserdata = {};
    this.usersFeedback = [];
    this.ansArray = [];

    this.selectedFeedback = feedback;
    this.FEEDBACKS.forEach(value => {
      if (value.customer_id == feedback.customer_id) {
        this.usersFeedback.push(value);
      }
    });
    this.usersFeedback.reverse();    
    this.usersFeedback = this.usersFeedback.slice(0,2);
  }

  sendReply() {
    // //console.log('sending reply...');
    this.feedbackForm.patchValue({
      client_id: this.user.data._id,
      customer_id: this.selectedFeedback.customer_id,
      name: this.selectedFeedback.customerData.name,
      phoneno: this.selectedFeedback.customerData.phoneno
    });
    // //console.log(this.feedbackForm.value);
    if (this.feedbackForm.invalid) {
      return;
    }
    this.http.post(this.baseUrl + '/feedback/feedbackreplied', this.feedbackForm.value)
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.alertService.success('Replied successfully!', false);
          this.selectedFeedback.replied = true;
        } else {
        }
      }, error => {
      });
    this.feedbackForm.reset({ client_id: "", customer_id: "", message: "", name: "", phoneno: "" });

  }
  getAnswer(controlname, selectedFeedback1) {
    // //console.log(controlname,selectedFeedback1)
    var x;
    for (x in selectedFeedback1) {
      // //console.log(x ,'==', controlname)
      if (x == controlname) {
        return selectedFeedback1[x]
      }
    }
  }


}
