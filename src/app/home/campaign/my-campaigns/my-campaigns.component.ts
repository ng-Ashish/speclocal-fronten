import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AlertService } from 'src/app/services/alert.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

interface Campaign {
  id?: number;
  created_date: any;
  message: string;
  customers: any;
}
@Component({
  selector: 'app-my-campaigns',
  templateUrl: './my-campaigns.component.html',
  styleUrls: ['./my-campaigns.component.css']
})
export class MyCampaignsComponent implements OnInit {

  CAMPAIGNS: Campaign[] = [];
  user: any;
  baseUrl: string = environment.baseUrl;
  viewCustomerList: any = [];

  page = 1;
  pageSize = 10;
  collectionSize = this.CAMPAIGNS.length;

  constructor(public http: HttpClient, public alertService: AlertService,public modalService: NgbModal) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    var email : string;
    let emailid;
    if(this.user.data.role=='emp'){
      emailid = this.user.data.adminemail;
    }else{
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/campaign/get/' + emailid).subscribe(res => {
      console.log(res);
      if (res['success']) {
        this.CAMPAIGNS = res['data'][0];
        var employee_id = this.user.data._id;
        console.log('employee id : ',employee_id)
        this.CAMPAIGNS = this.CAMPAIGNS.filter((campaign:any)=>{
          return campaign.client_id == employee_id
        })
        this.collectionSize = this.CAMPAIGNS.length;
        this.CAMPAIGNS.reverse();
      }
    }, error => {
      // this.alertService.error('Error!', false);
    });
  }

  get campaigns(): Campaign[] {
    return this.CAMPAIGNS
      .map((campaign, i) => ({ id: i + 1, ...campaign }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  viewList(content,list: any) {
    this.viewCustomerList = list;
    this.modalService.open(content, { centered: true });
  }

}
