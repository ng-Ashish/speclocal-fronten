import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AlertService } from 'src/app/services/alert.service';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

interface Customer {
  id?: number;
  name: string;
  dialCode?: string;
  phoneno: number;
  email: string;
  occupation: string;
  birthdate: string;
  anniversary: string;
  address: string;
  campaignmsg: boolean;
  is_feedback: boolean;
  selected?: boolean;
}

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

  // @ViewChild('addCampaignForm2') addCampaignForm2: FormGroupDirective; 
  isAll: boolean = false;
  save: boolean = false;
  templates: any = [];
  submitted: boolean = false;
  CUSTOMERS: Customer[] = [];
  campaignCustomers: Customer[] = [];
  user: any;
  baseUrl: string = environment.baseUrl;
  selectedCustomers: Customer[] = [];
  addCampaignForm: FormGroup;

  //paginator 
  p = 1;
  itemsPerPage = 10;
  page: any;
  collectionSize = this.campaignCustomers.length;


  //change
  selectedTemplate: any = {};
  templateType = '';
  template_id = '';
  customTemplateFlag: boolean = false;
  isSaveTemplate: boolean = false;
  greeting: string = '';
  constructor(
    public http: HttpClient,
    public alertService: AlertService,
    public userService: UserService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.addCampaignForm = new FormGroup({
      messagetype: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
      templatename: new FormControl(''),
      greeting: new FormControl('')
    });
    if (this.user == 'gurujipune81@gmail.com') {
      this.addCampaignForm.patchValue({
        greeting: 'Namskar '
      });
    } else {
      this.addCampaignForm.patchValue({
        greeting: 'Hi '
      });
    }
    this.getCustomers();
    this.formValueChanges();
    this.getCampaignTemlates();
  }

  formValueChanges() {
    this.addCampaignForm.controls['messagetype'].valueChanges.subscribe(
      (selectedValue) => {
        console.log('selectedValue >>', selectedValue)
        if (selectedValue == 'Feedback') {
          var selectedTemplate = this.templates.filter(temp => {
            return temp.templatename == selectedValue
          })
          this.templateType = selectedValue
          this.addCampaignForm.patchValue({
            message: selectedTemplate[0].messagebody
          });
          this.campaignCustomers = this.CUSTOMERS.filter((element) => element.campaignmsg == false);
          this.collectionSize = this.campaignCustomers.length;
        } else if (selectedValue == 'own') {
          this.templateType = selectedValue;
          this.customTemplateFlag = true
          this.addCampaignForm.patchValue({ message: "" });
          this.campaignCustomers = this.CUSTOMERS;
          this.collectionSize = this.campaignCustomers.length;
        } else if (selectedValue == "") {
          this.campaignCustomers = [];
          this.collectionSize = this.campaignCustomers.length;
        } else {
          var selectedTemplate = this.templates.filter(temp => {
            return temp.templatename == selectedValue
          })
          // console.log('selectedTemplate[0] >>', selectedTemplate[0])
          this.templateType = selectedValue
          this.addCampaignForm.patchValue({
            message: selectedTemplate[0].messagebody,
            type: selectedValue
          });
          this.campaignCustomers = this.CUSTOMERS;
          this.collectionSize = this.campaignCustomers.length;
        }

        //iterate loop for check box setup
        this.campaignCustomers.forEach(value => {
          value.selected = false;
        });
        // console.log(this.CUSTOMERS)
      }
    );
  }

  getCampaignTemlates() {
    let emailid = ''
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/templatemessage/gettemplatemessage/' + emailid)
      .subscribe((res: any) => {
        console.log('res >>', res);
        // if (res['success']) {
        //   this.templates = res['data'];
        // }
        if (res.success) {
          this.templates = res.data;
        }
      });
  }
  getCustomers() {
    this.http.get(this.baseUrl + '/customer/get/' + this.user.data.email).subscribe(res => {
      if (res['success']) {
        // console.log(res['data']['customers']);
        this.CUSTOMERS = res['data']['customers'];

        const result = [];
        const map = new Map();
        for (const item of this.CUSTOMERS) {
          if (!map.has(item.phoneno)) {
            map.set(item.phoneno, true);    // set any value to Map
            result.push(item);
          }
        }
        this.CUSTOMERS = result;
      }
      else {
        this.CUSTOMERS = [];
      }
    }, error => {
      // this.alertService.error('Error!', false);
    });

  }


  saveTemplate(flag: any) {
    // console.log('flag :', flag)
    flag = !flag;
    // debugger;
    if (flag) {
      this.isSaveTemplate = flag;
      this.addCampaignForm.get('templatename').setValidators([Validators.required]);
    } else {
      this.isSaveTemplate = flag;
      this.addCampaignForm.get('templatename').clearValidators();
    }
    this.addCampaignForm.get('templatename').updateValueAndValidity();
  }
  get campaign() { return this.addCampaignForm.controls; }
  addCampaign(flag?: any) {
    // console.log('add campaign flag >>', flag)
    this.submitted = true;
    if (this.addCampaignForm.invalid) {
      return;
    }
    if (this.campaignCustomers.length == 0) {
      this.alertService.warning('Add atleast one customer first!', false);
      return;
    }
    this.campaignCustomers.forEach(value => {
      if (value.selected) {
        this.selectedCustomers.push(value);
      }
    });

    if (this.selectedCustomers.length == 0) {
      this.alertService.warning('Select atleast one customer!', false);
      return;
    }

    if (this.userService.msgcredits < this.selectedCustomers.length) {
      this.alertService.warning("Can't add campaign! No sufficient SMS credits available.");
      this.addCampaignForm.reset({ message: "" });
      return;
    }
    var templateData = this.templates.filter(temp => {
      return temp.templatename == this.templateType
    });
    let emailid = ''
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }

    let data = {
      email: emailid,
      client_id: this.user.data._id,
      type: templateData[0].templatename,
      templateId: templateData[0]._id,
      greeting: this.addCampaignForm.get('greeting').value,
      message: this.addCampaignForm.get('message').value,
      customers: this.campaignCustomers.filter((res) => { return res.selected === true }),
      msgDeduct: (Math.ceil(this.addCampaignForm.get('message').value.length / 160))
    }

    if (this.addCampaignForm.get('greeting').value == '') {
      this.alertService.success('Please set Greeting you want', false);
      this.getCustomers();
      this.getCampaignTemlates()
    } else {
      this.http.post(this.baseUrl + '/campaign/post', data).subscribe(res => {
        if (res['success']) {
          this.userService.getCredits();
          const ele = document.getElementById("selectAllCheckbox") as HTMLInputElement;
          ele.checked = false;
          this.campaignCustomers = [];
          this.collectionSize = this.campaignCustomers.length;
          this.getCustomers();
          this.getCampaignTemlates()          
          this.alertService.success('Message sent successfully!', false);
        } else {
          // this.alertService.error('Something went wrong!', false);
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
    }

    this.addCampaignForm.reset({ messagetype: "", message: "", templatename: "", });
    this.addCampaignForm.patchValue({
      greeting: 'Hi '
    });
    this.isAll = false;
    this.save = false;
    this.submitted = false;
  }
  msgDeductCount() {
    return Math.ceil(this.addCampaignForm.get('message').value.length / 160);
  }
  selectRow(flag: any, index: number) {
    this.campaignCustomers.map((data: any, i: number) => {
      if (index === i) {
        this.campaignCustomers[i].selected = !this.campaignCustomers[i].selected
      }
    })
  }  

  saveTemplatetoDB(data) {
    if (data.keyCode === 13) {  //checks whether the pressed key is "Enter"
      var obj = {
        client_id: this.user.data._id,
        templatename: this.addCampaignForm.get('templatename').value,
        messagebody: this.addCampaignForm.get('message').value,
      }
      this.http.post(this.baseUrl + '/templatemessage/addtemplatemessage', obj).subscribe(res => {
        // console.log(res);
        if (res['success']) {
          this.alertService.success('Template saved successfully!', false);
          this.getCampaignTemlates();
          // this.addCampaignForm.reset()
          this.addCampaignForm.get('templatename').reset();
          this.addCampaignForm.get('message').reset();
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
      // console.log('custome message')
    }

  }
  absoluteIndex(indexOnPage: number): number {
    return this.itemsPerPage * (this.p - 1) + indexOnPage;
  }
  pageChanged(event) {
    // console.log('page :',event)
    this.p = event;
  }
  change(index: number) {
    var actualindex = this.absoluteIndex(index);
    console.log(index, actualindex)
    this.campaignCustomers.forEach((d, index) => {
      if (actualindex == index) {
        if (this.campaignCustomers[actualindex].selected) {
          this.campaignCustomers[actualindex].selected = false;
        } else {
          this.campaignCustomers[actualindex].selected = true;
        }
      }
    })
    // this.CUSTOMERS.forEach((d, index) => {
    //   if (actualindex == index) {
    //     if (this.CUSTOMERS[actualindex].selected) {
    //       this.CUSTOMERS[actualindex].selected = false
    //     } else {
    //       this.CUSTOMERS[actualindex].selected = true
    //     }
    //   }
    // })

  }

  selectAll(flag) {
    this.isAll = !flag;

    if (this.isAll) {
      this.campaignCustomers.forEach((d, index) => {
        this.campaignCustomers[index].selected = true;
      })
    } else {
      this.campaignCustomers.forEach((d, index) => {
        this.campaignCustomers[index].selected = false;
      })
    }
  }
}
