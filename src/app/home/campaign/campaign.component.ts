import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { GoogleService } from 'src/app/services/google.service';
import { FbService } from 'src/app/services/fb.service';
import { ZomatoService } from 'src/app/services/zomato.service';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {

  @ViewChild('imageSelect') imageFile: ElementRef;
  image = null;
  images: string[] = [];
  filenames : string[] = [];
  filename : String[] = []
  hide = true;
  public uploader: FileUploader = new FileUploader({ url: "https://speclocal.com:4500/api/uploadimage", itemAlias: 'image' });

  campaigningmode: string;
  socialcampaigningmode: string;

  linkedGoogleAccountLocation: any;

  ctaPostForm: FormGroup;
  offerPostForm: FormGroup;
  productPostForm: FormGroup;
  eventPostForm: FormGroup;
  facebookPostForm: FormGroup;

  submittedCtaPostForm: boolean = false;
  submittedOfferPostForm: boolean = false;
  submittedProductPostForm: boolean = false;
  submittedEventPostForm: boolean = false;
  submittedFacebookPostForm: boolean = false;

  startDate: any;
  endDate: any;
  startTime: any = { hour: 0, minute: 0 };
  endTime: any = { hour: 23, minute: 59 };
  meridian = true;
  user: any = {};
  sms : boolean = false;
  baseUrl: string = environment.baseUrl;
  constructor(
    public googleService: GoogleService,
    public fbService: FbService,
    public zomatoService: ZomatoService,
    public alertService: AlertService,
    public http: HttpClient,
    public router: Router,
    public userservice : UserService,
    public sharedservice : SharedService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.invalidUserStatus()
    this.linkedGoogleAccountLocation = this.googleService.linkedGoogleAccountLocation;
    this.sms = this.user.data.smscampaining;
    this.getusersSmsservicestatus();
    
    var emailid = '';
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }    
    this.sharedservice.getOneFeedbackForm(emailid);    
    if(!this.sharedservice.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.user.data.email).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }
  ngOnInit() {
    this.ctaPostForm = new FormGroup({
      languageCode: new FormControl('en-US'),
      summary: new FormControl('', Validators.required),
      topicType: new FormControl('STANDARD'),
      callToAction: new FormGroup({
        actionType: new FormControl(''),
        url: new FormControl('')
      }),
      media: new FormGroup({
        mediaFormat: new FormControl('PHOTO'),
        sourceUrl: new FormControl('')
      })
    });

    this.offerPostForm = new FormGroup({
      languageCode: new FormControl('en-US'),
      summary: new FormControl('', Validators.required),
      topicType: new FormControl('OFFER'),
      offer: new FormGroup({
        couponCode: new FormControl(''),
        redeemOnlineUrl: new FormControl(''),
        termsConditions: new FormControl('')
      }),
      event: new FormGroup({
        title: new FormControl('', Validators.required),
        schedule: new FormGroup({
          startDate: new FormGroup({
            year: new FormControl('', Validators.required),
            month: new FormControl('', Validators.required),
            day: new FormControl('', Validators.required)
          }),
          endDate: new FormGroup({
            year: new FormControl('', Validators.required),
            month: new FormControl('', Validators.required),
            day: new FormControl('', Validators.required)
          }),
          startTime: new FormGroup({
            hours: new FormControl(0),
            minutes: new FormControl(0),
            seconds: new FormControl(0),
            nanos: new FormControl(0)
          }),
          endTime: new FormGroup({
            hours: new FormControl(23),
            minutes: new FormControl(0),
            seconds: new FormControl(0),
            nanos: new FormControl(0)
          }),
        })
      }),
      media: new FormGroup({
        mediaFormat: new FormControl('PHOTO'),
        sourceUrl: new FormControl('')
      })
    });

    this.productPostForm = new FormGroup({
      languageCode: new FormControl('en-US'),
      summary: new FormControl('', Validators.required),
      topicType: new FormControl('PRODUCT'),
      product: new FormGroup({
        productName: new FormControl('', Validators.required),
        lowerPrice: new FormGroup({
          currencyCode: new FormControl('INR'),
          units: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]+$/)]),
          nanos: new FormControl(0)
        }),
        upperPrice: new FormGroup({
          currencyCode: new FormControl('INR'),
          units: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]+$/)]),
          nanos: new FormControl(0)
        }),
      }),
      callToAction: new FormGroup({
        actionType: new FormControl(''),
        url: new FormControl('')
      }),
      media: new FormGroup({
        mediaFormat: new FormControl('PHOTO'),
        sourceUrl: new FormControl('', Validators.required)
      })
    });

    this.eventPostForm = new FormGroup({
      languageCode: new FormControl('en-US'),
      summary: new FormControl('', Validators.required),
      topicType: new FormControl('EVENT'),
      callToAction: new FormGroup({
        actionType: new FormControl(''),
        url: new FormControl('')
      }),
      event: new FormGroup({
        title: new FormControl('', Validators.required),
        schedule: new FormGroup({
          startDate: new FormGroup({
            year: new FormControl('', Validators.required),
            month: new FormControl('', Validators.required),
            day: new FormControl('', Validators.required)
          }),
          endDate: new FormGroup({
            year: new FormControl('', Validators.required),
            month: new FormControl('', Validators.required),
            day: new FormControl('', Validators.required)
          }),
          startTime: new FormGroup({
            hours: new FormControl(0),
            minutes: new FormControl(0),
            seconds: new FormControl(0),
            nanos: new FormControl(0)
          }),
          endTime: new FormGroup({
            hours: new FormControl(23),
            minutes: new FormControl(59),
            seconds: new FormControl(0),
            nanos: new FormControl(0)
          }),
        })
      }),
      media: new FormGroup({
        mediaFormat: new FormControl('PHOTO'),
        sourceUrl: new FormControl('')
      })
    });

    this.facebookPostForm = new FormGroup({
      message: new FormControl('', Validators.required),
      source: new FormControl('')
      // link: new FormControl('')
    });

    this.uploader.onAfterAddingFile = (file) => { 
      //console.log("file : " , file._file.name)
      this.filename.push(file._file.name)
      this.filenames.push(file._file.name);      
      file.withCredentials = false 
    };
    this.uploader.onCompleteItem = (item: any, res: any, status: any, headers: any) => {
      this.image = res;
      // //console.log(res);
      this.images.push(res);
      // this.ctaPostForm.get('media').get('sourceUrl').setValue(res);
      // this.offerPostForm.get('media').get('sourceUrl').setValue(res);
      // this.eventPostForm.get('media').get('sourceUrl').setValue(res);
      // this.productPostForm.get('media').get('sourceUrl').setValue(res);
      this.facebookPostForm.get('source').setValue(res);
    }

    this.formControlValueChanged();
    this.getusersSmsservicestatus();
  }
  getusersSmsservicestatus() {
    let emailid;
    if(this.user.data.role=='emp'){
      emailid = this.user.data.adminemail;
    }else{
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        this.sms = res['data'].smscampaining
        this.userservice.sms = res['data'].smscampaining;
        //console.log("smscampaining : ",this.userservice.sms)
      }
    })
  }
  formControlValueChanged() {
    this.ctaPostForm.get('callToAction').get('actionType').valueChanges.subscribe(
      (mode: string) => {
        if (mode && mode != "CALL") {
          this.ctaPostForm.get('callToAction').get('url').setValidators([Validators.required]);
        } else {
          this.ctaPostForm.get('callToAction').get('url').clearValidators();
        }
        this.ctaPostForm.get('callToAction').get('url').updateValueAndValidity();
      });

    this.productPostForm.get('callToAction').get('actionType').valueChanges.subscribe(
      (mode: string) => {
        if (mode && mode != "CALL") {
          this.productPostForm.get('callToAction').get('url').setValidators([Validators.required]);
        } else {
          this.productPostForm.get('callToAction').get('url').clearValidators();
        }
        this.productPostForm.get('callToAction').get('url').updateValueAndValidity();
      });

    this.eventPostForm.get('callToAction').get('actionType').valueChanges.subscribe(
      (mode: string) => {
        if (mode && mode != "CALL") {
          this.eventPostForm.get('callToAction').get('url').setValidators([Validators.required]);
        } else {
          this.eventPostForm.get('callToAction').get('url').clearValidators();
        }
        this.eventPostForm.get('callToAction').get('url').updateValueAndValidity();
      });

  }

  get cta() { return this.ctaPostForm.controls; }

  postCtaPost() {
    this.submittedCtaPostForm = true;
    if (this.ctaPostForm.invalid) {
      return;
    }
    // //console.log(this.ctaPostForm.value);
    let data = this.ctaPostForm.value;
    if (this.ctaPostForm.get('callToAction').get('actionType').value == "") {
      delete data['callToAction'];
    }
    if (this.ctaPostForm.get('media').get('sourceUrl').value == "") {
      delete data['media'];
    }
    if (this.ctaPostForm.get('callToAction').get('actionType').value == "CALL") {
      delete data['callToAction']['url'];
    }
    this.googleService.quickPost(this.googleService.linked_location.name, data);
    this.ctaPostForm.reset({
      "languageCode": "en-US",
      "summary": "",
      "topicType": "STANDARD",
      "callToAction": {
        "actionType": "",
        "url": "",
      },
      "media": {
        "mediaFormat": "PHOTO",
        "sourceUrl": "",
      }
    });
    this.submittedCtaPostForm = false;
    this.image = null;
    this.alertService.success('Posted on google sucessfully!', false);
    location.reload();
  }

  get offer() { return this.offerPostForm.controls; }

  postOfferPost() {
    this.submittedOfferPostForm = true;
    if (this.startDate && this.endDate) {
      this.offerPostForm.patchValue({
        "event": {
          "schedule": {
            "startDate": this.startDate,
            "endDate": this.endDate,
            "startTime": {
              "hours": this.startTime.hour,
              "minutes": this.startTime.minute,
              "seconds": 0,
              "nanos": 0
            },
            "endTime": {
              "hours": this.endTime.hour,
              "minutes": this.endTime.minute,
              "seconds": 0,
              "nanos": 0
            }
          }
        }
      });
    }
    if (this.offerPostForm.invalid) {
      return;
    }
    // //console.log(this.offerPostForm.value);
    let data = this.offerPostForm.value;
    if (this.offerPostForm.get('media').get('sourceUrl').value == "") {
      delete data['media'];
    }
    this.googleService.quickPost(this.googleService.linked_location.name, data);
    this.offerPostForm.reset({
      "languageCode": "en-US",
      "summary": "",
      "topicType": "OFFER",
      "event": {
        "schedule": {
          "startDate": {
            "year": "",
            "month": "",
            "date": ""
          },
          "endDate": {
            "year": "",
            "month": "",
            "date": ""
          },
          "startTime": {
            "hours": 0,
            "minutes": 0,
            "seconds": 0,
            "nanos": 0
          },
          "endTime": {
            "hours": 23,
            "minutes": 59,
            "seconds": 0,
            "nanos": 0
          }
        }
      },
      "offer": {
        "couponCode": "",
        "redeemOnlineUrl": "",
        "termsConditions": ""
      },
      "media": {
        "mediaFormat": "PHOTO",
        "sourceUrl": "",
      }
    });
    this.image = null;
    this.startDate = undefined;
    this.endDate = undefined;
    this.startTime = { hour: 0, minute: 0 };
    this.endTime = { hour: 23, minute: 59 };
    this.submittedOfferPostForm = false;
    this.alertService.success('Posted on google successfully!', false);
    location.reload();
  }

  get product() { return this.productPostForm.controls; }

  postProductPost() {
    this.submittedProductPostForm = true;
    if (this.productPostForm.invalid) {
      return;
    }
    // //console.log(this.productPostForm.value);
    let data = this.productPostForm.value;
    if (this.productPostForm.get('callToAction').get('actionType').value == "") {
      delete data['callToAction'];
    }
    if (this.productPostForm.get('callToAction').get('actionType').value == "CALL") {
      delete data['callToAction']['url'];
    }
    if (this.productPostForm.get('media').get('sourceUrl').value == "") {
      delete data['media'];
    }
    this.googleService.quickPost(this.googleService.linked_location.name, data);
    this.productPostForm.reset({
      "languageCode": "en-US",
      "summary": "",
      "topicType": "PRODUCT",
      "product": {
        "productName": "",
        "lowerPrice": {
          "currencyCode": "INR",
          "units": "",
          "nanos": 0
        },
        "upperPrice": {
          "currencyCode": "INR",
          "units": "",
          "nanos": 0
        }
      },
      "callToAction": {
        "actionType": "",
        "url": "",
      },
      "media": {
        "mediaFormat": "PHOTO",
        "sourceUrl": "",
      }
    });
    this.image = null;
    this.submittedProductPostForm = false;
    this.alertService.success('Posted on google successfully!', false);
    location.reload();
  }

  get event() { return this.eventPostForm.controls; }

  postEventPost() {
    this.submittedEventPostForm = true;
    if (this.startDate && this.endDate) {
      this.eventPostForm.patchValue({
        "event": {
          "schedule": {
            "startDate": this.startDate,
            "endDate": this.endDate,
            "startTime": {
              "hours": this.startTime.hour,
              "minutes": this.startTime.minute,
              "seconds": 0,
              "nanos": 0
            },
            "endTime": {
              "hours": this.endTime.hour,
              "minutes": this.endTime.minute,
              "seconds": 0,
              "nanos": 0
            }
          }
        }
      });
    }
    if (this.eventPostForm.invalid) {
      return;
    }
    // //console.log(this.eventPostForm.value);
    let data = this.eventPostForm.value;
    if (this.eventPostForm.get('callToAction').get('actionType').value == "") {
      delete data['callToAction'];
    }
    if (this.eventPostForm.get('callToAction').get('actionType').value == "CALL") {
      delete data['callToAction']['url'];
    }
    if (this.eventPostForm.get('media').get('sourceUrl').value == "") {
      delete data['media'];
    }
    this.googleService.quickPost(this.googleService.linked_location.name, data);
    this.eventPostForm.reset({
      "languageCode": "en-US",
      "summary": "",
      "topicType": "EVENT",
      "event": {
        "schedule": {
          "startDate": {
            "year": "",
            "month": "",
            "date": ""
          },
          "endDate": {
            "year": "",
            "month": "",
            "date": ""
          },
          "startTime": {
            "hours": 0,
            "minutes": 0,
            "seconds": 0,
            "nanos": 0
          },
          "endTime": {
            "hours": 23,
            "minutes": 59,
            "seconds": 0,
            "nanos": 0
          }
        }
      },
      "callToAction": {
        "actionType": "",
        "url": "",
      },
      "media": {
        "mediaFormat": "PHOTO",
        "sourceUrl": "",
      }
    });
    this.image = null;
    this.startDate = undefined;
    this.endDate = undefined;
    this.startTime = { hour: 0, minute: 0 };
    this.endTime = { hour: 23, minute: 59 };
    this.submittedEventPostForm = false;
    this.alertService.success('Posted on google successfully!', false);
    location.reload();
  }

  get fb() { return this.facebookPostForm.controls; }

  postOnFacebook() {
    this.submittedFacebookPostForm = true;
    if (this.facebookPostForm.invalid) {
      return;
    }
    let data = { images: this.images, ...this.facebookPostForm.value };
    // this.fbService.quickPost(this.fbService.linked_page, this.facebookPostForm.value);
    this.fbService.quickPostWithMultipleImages(this.fbService.linked_page, data);
    this.image = null;    
    this.facebookPostForm.reset({
      "message": "",
      "source": ""
    });
    this.submittedFacebookPostForm = false;
    this.images = [];
    this.alertService.success('Posted on facebook successfully!', false);
  }
  removeimg(index){
    if (index > -1) {
      this.images.splice(index, 1);
      this.filenames.splice(index, 1)
    }
    //console.log('file', this.filenames)

  }
  removeImg(){
    this.image = '';
    this.filename = [];
    this.imageFile.nativeElement.value = '';
    this.ctaPostForm.get('media').get('sourceUrl').reset();
    this.offerPostForm.get('media').get('sourceUrl').reset();
    this.eventPostForm.get('media').get('sourceUrl').reset();
    this.productPostForm.get('media').get('sourceUrl').reset();
  }
  selectCampaigningMode(mode: string) {
    this.campaigningmode = mode;
  }

  selectSocialCampaigningMode(mode: string) {
    this.image = '';
    this.images = [];
    this.filenames =[];
    this.socialcampaigningmode = mode;
  }
  
}
