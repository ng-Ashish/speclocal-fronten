import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {

  baseUrl: string = environment.baseUrl;
  submitted: boolean = false;
  issueForm: FormGroup;
  user: any;
  constructor(public alertService: AlertService, public http: HttpClient, public router: Router) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.invalidUserStatus()
  }
  /**
   * Function to check the user status is valid or not , 
   * if not then redirect user to login page
   */
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.user.data.email).subscribe(res => {
      console.log("res : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.issueForm = new FormGroup({
      email: new FormControl(this.user.data.email, Validators.required),
      issue_related_to: new FormControl('', Validators.required),
      issue_description: new FormControl('', Validators.required)
    });
  }

  get i() { return this.issueForm.controls; }

  /**
   * This function for sending mail related to issue facing by user
   */
  onSubmit() {
    this.submitted = true;
    if (this.issueForm.invalid) {
      return;
    }
    // console.log(this.issueForm.value);
    this.http.post(this.baseUrl + '/issue/sendmail', this.issueForm.value).subscribe(res => {
      console.log(res);
      if (res['success']) {
        this.issueForm.reset({ issue_related_to: "", issue_description: "" });
        this.submitted = false;
        this.alertService.success('Issue submitted successfully! We will get back to you shortly.', false);
      } else {
        this.alertService.error(res['msg'], false);
      }
    }, error => {
      // this.alertService.error('Error!', false);
    })

  }
}
