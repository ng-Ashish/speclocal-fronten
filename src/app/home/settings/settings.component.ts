import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FbService } from 'src/app/services/fb.service';
import { GoogleService } from 'src/app/services/google.service';
import { ZomatoService } from 'src/app/services/zomato.service';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user.service';
import { FeedbackformserviceService } from 'src/app/services/feedbackformservice.service';
import { Router } from '@angular/router';
declare var alertify: any;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  user: any;
  baseUrl: string = environment.baseUrl;

  //role based variables
  role: string;
  roleEmp: boolean = false;
  employees: any;
  showsetting: boolean = false;
  //change pass and setgoals variables                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
  submitted: boolean = false;
  submittedPassword: boolean = false
  changePasswordForm: FormGroup;
  setgoalsForm: FormGroup;
  invitationForm: FormGroup;

  //profile form variables
  submittedProfile: boolean = false;
  profileForm: FormGroup;
  profileData: any;
  editMode: boolean = false;
  goal: number;
  invitation: number;


  //question feedback
  submittedForm: boolean = false;
  isoption: boolean;
  radioOption = [];

  // Link Accounts variables
  linkedFacebookPage: any;
  linkedGoogleAccount: any;
  linkedGoogleAccountLocation: any;

  zomatoRestaurantForm: FormGroup;
  //update service variables
  serviceForm: FormGroup;
  //Request service variables
  requestEmployeeForm: FormGroup;
  buySmsCreditsForm: FormGroup;
  feedbackForm: FormGroup;
  questions: any[] = [];
  oneFeedbackForm;
  feedbackFormData = [];
  email: string;
  formId: string;

  Radio: boolean = false;
  Ratings: boolean = false;
  CheckBox: boolean = false;
  Textarea: boolean = false;
  submittedFeedback: boolean = false
  showFeedbackForm: boolean = true;
  // code to add checkboxes for sms services
  checks = [
    { id: 1, name: 'Birthday SMS' },
    { id: 2, name: 'Anniversary SMS' },
    { id: 3, name: 'SMS Campaign' }
  ];

  // SMS Credits
  // buySmsCreditsForm: FormGroup;
  credits: number = 0;
  smsServices: [];
  birthday: boolean = false
  anniversary: boolean = false
  smscampanign: boolean = false
  facebook: boolean = false
  google: boolean = false
  zomato: boolean = false
  platformname: any;

  //variables for platform logo url
  facebooklogourl: String
  googlelogourl: String
  zomatologourl: String

  opensettings: boolean = true;

  //Number Of Employees Request variables
  noOfEmployees: number;
  employeeRequestStatus: boolean = true;
  smsRequestStatus: boolean = true;
  msgCreditRequestStatus: boolean = true;
  employeeCount: number;
  noofemployee: number;
  platformsalldata: any = []
  platformRequestStatus: boolean = true;

  constructor(
    public modalService: NgbModal,
    public fb: FormBuilder,
    public alertService: AlertService,
    public http: HttpClient,
    public fbService: FbService,
    public googleService: GoogleService,
    public zomatoService: ZomatoService,
    public sharedService: SharedService,
    public userService: UserService,
    public feedbackformService: FeedbackformserviceService,
    private router: Router) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.serviceForm = this.fb.group({
      checks: new FormArray([])
    });
    this.addCheckboxes();
    this.feedbackform();
    this.isoption = false;
    this.feedbackForm.controls['selectOption'].valueChanges.subscribe(selected => {
      // ////console.log(selected)
      if (selected == 'Radio') {
        this.isoption = true;
      } else {
        this.isoption = false;
      }
    });
    this.getFeedbackForm();
    this.getusersSmsservicestatus();

    var emailid = '';
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.sharedService.getOneFeedbackForm(emailid);
    if (!this.sharedService.isFeedbakTemplate) {
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  ngOnInit() {
    this.role = this.user.data.role;
    if (this.role == 'emp') {
      this.roleEmp = true;
    } else {
      this.roleEmp = false;
    }
    this.email = this.user.data.email;
    // this.getuser();
    this.userService.getCredits();
    this.setgoalsform();
    this.changepasswordform();
    this.profileform();
    this.getCredits();
    this.buysmscreditsform();
    this.getEmployees();
    this.getsmsServices();

    this.requestemployeeform();

    this.zomatoRestaurantForm = new FormGroup({
      restaurant_id: new FormControl('16774318', Validators.required)
    });
    this.getnoOfEmployees();
    this.getStatusOfEmployeeRequest();
    this.getStatusOfSmsRequest();
    this.getStatusMsgCreditRequest();
    this.getEmployeeCount();
    this.getplatformwithalldetails();
    this.getplatforms();
    // this.getallavailableplatforms();
    // this.getStatusOfPlatformRequest();
  }

  /**
   * This function is used for get the status of sms services
   * of user
   */
  getusersSmsservicestatus() {
    this.http.get(this.baseUrl + '/user/user/' + this.user.data.email).subscribe(res => {
      // console.log("res  : ", res);
      if (res["success"]) {
        this.birthday = res['data'].birthday
        this.anniversary = res['data'].anniversary
        this.smscampanign = res['data'].smscampaining
        console.log('sms campaign >', this.smscampanign)
        console.log('template :', this.feedbackTemplate.length)
        if (this.feedbackTemplate.length == 0) {
          this.smscampanign = false;
          this.userService.sms = false;
        } else {
          this.smscampanign = true;
          this.userService.sms = res['data'].smscampaining;
        }
        this.serviceForm = this.fb.group({
          checks: new FormArray([
            new FormControl(this.birthday),
            new FormControl(this.anniversary),
            new FormControl(this.smscampanign)
          ])
        });
      }
    })
  }

  /**
   * This function is get the platforms with all details
   */
  getplatformwithalldetails() {
    this.userService.getplatformwithalldetails().subscribe(res => {
      // //console.log("res : ", res)
      if (res['success']) {
        this.platformsalldata = res['data']
      }
    })
  }
  /**
   * This function is used to platforms that user have
   */
  getplatforms() {
    this.userService.getuser().subscribe(res => {
      // //console.log("res : ", res)
      if (res['success']) {
        let platforms = res['data']['platformname']
        // //console.log("platforms : ", platforms)
        if (platforms.length > 0) {
          platforms.forEach(element => {
            if (element == 'facebook') {
              this.facebook = true
              this.setlogo('facebook');
            }
            if (element == 'google') {
              this.google = true
              this.setlogo('google');
            }
            if (element == 'zomato') {
              this.zomato = true
              this.setlogo('zomato');
            }
          });
        } else {
          this.zomato = false;
          this.google = false;
          this.facebook = false;
        }
        this.getplatformwithalldetails()
      }
    })

  }

/**
 * 
 * @param platform : holds the platform name
 * This function is used for setting the logo to pericular platform
 */
  setlogo(platform) {
    this.userService.getplatformDetailsByName(platform).subscribe(res => {
      //console.log("response : ", res['data']);
      if (res['success']) {
        if (res['data']['platformName'] == 'facebook') {
          this.facebooklogourl = res['data']['platformLogoURL']
        }
        if (res['data']['platformName'] == 'google') {
          this.googlelogourl = res['data']['platformLogoURL']
        }
        if (res['data']['platformName'] == 'zomato') {
          this.zomatologourl = res['data']['platformLogoURL']
        }
      }
    })
  }
  /*
  * getEmployeeCount : for getting count of added employees of perticular admin
  */
  getEmployeeCount() {
    this.userService.getEmployeeCount().subscribe(res => {
      if (res['success']) {
        this.employeeCount = res['data'];
        //console.log(this.employeeCount)
        this.userService.getnoOfEmployees().subscribe(res => {
          //  ////console.log("res vaishu",res)
          if (res['success']) {
            this.noofemployee = res['data'].noOfEmployees;
            //  ////console.log(this.noofemployee)

          } else {
            this.noofemployee = 0;
          }
        })

      }
    })
  }
  private addCheckboxes() {
    this.getusersSmsservicestatus()
  }
  /**
   * This function is for getting the sms services of perticular
   * user
   */
  getsmsServices() {
    this.userService.getsmsServices().subscribe(res => {
      ////console.log("getsms service : ", res);
      if (res['success']) {
        this.smsServices = res['data']['smsService'];
      }
    }, error => {
      // this.alertService.error('Error getting SMS Services!', false);
    })
  }
  get sms() { return this.buySmsCreditsForm.controls; }

  buysmscreditsform() {
    this.buySmsCreditsForm = new FormGroup({
      count: new FormControl('', Validators.required)
    });
  }
  /**
   * request to increse no of employee count of user
   */
  requestemployeeform() {
    this.requestEmployeeForm = new FormGroup({
      noOfEmployees: new FormControl('', Validators.required),
      email: new FormControl('')
    })
  }

  //for getting credits of user
  getCredits() {
    this.http.get(this.baseUrl + '/user/user/' + this.user.data.email).subscribe(res => {
      if (res['success']) {
        this.credits = res['data']['msgcredit'];
      }
    }, error => {
      // this.alertService.error('Error getting SMS credits!', false);
    });
  }
 //request for credits
  buyCredits() {
    if (this.buySmsCreditsForm.invalid) {
      return;
    }
    let data = {
      email: this.user.data.email,
      msgcredit: this.buySmsCreditsForm.controls['count'].value
    }

    this.http.post(this.baseUrl + '/msgcredit/post', data).subscribe(res => {
      // ////console.log(res);
      if (res['success']) {
        this.alertService.success('Your request to buy SMS credits is sent successfully!', false);
        this.modalService.dismissAll();
        this.buySmsCreditsForm.reset({ count: "" });
        this.msgCreditRequestStatus = false;
      }
    }, error => {
      // this.alertService.error('Error!', false);
    });
    this.buySmsCreditsForm.reset({ count: "" });
  }

  /**
   * request for sms service
   */
  requestSmsService() {
    if (this.serviceForm.invalid) {
      return;
    }
    const selectedServices = this.serviceForm.value.checks
      .map((v, i) => v ? this.checks[i].name : null)
      .filter(v => v !== null);
    ////console.log(selectedServices);
    //////console.log(" this.serviceForm.value", this.serviceForm.value.checks)

    let data = {
      email: this.user.data.email,
      // smsService: [
      firstname: this.user.data.firstname,
      lastname: this.user.data.lastname,
      birthday: this.serviceForm.value.checks[0],
      anniversary: this.serviceForm.value.checks[1],
      smscampaign: this.serviceForm.value.checks[2]
      // ]
    }
    //////console.log("data : ", data);
    this.http.post(this.baseUrl + '/smsservice/smsrequest', data).subscribe(result => {
      //////console.log("result : ", result);
      if (result['success']) {
        this.alertService.success('Your request to SMS service is sent successfully!', false);
        this.smsRequestStatus = false;
      }
    });

  }

  //set goal form goes here
  setgoalsform() {
    this.setgoalsForm = new FormGroup({
      email: new FormControl('', Validators.required),
      goal: new FormControl('', Validators.required),
      invitation: new FormControl('', Validators.required),
      adminemail: new FormControl(this.user.data.email)
    });
  }
 // change password form
  changepasswordform() {
    this.changePasswordForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('', Validators.required),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }
//profile form 
  profileform() {
    this.profileForm = new FormGroup({
      firstname: new FormControl({ value: this.user.data.firstname, disabled: true }, [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)]),
      lastname: new FormControl({ value: this.user.data.lastname, disabled: true }, [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)]),
      email: new FormControl({ value: this.user.data.email, disabled: true }, Validators.required),
      contactno: new FormControl({ value: this.user.data.contactno, disabled: true }, Validators.required),
      businessname: new FormControl({ value: this.user.data.businessname, disabled: true }, Validators.required)
    });
  }

  //modal for request sms service
  openRequestSmsModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {

    }, (reason) => { this.buySmsCreditsForm.reset({ count: "" }); });
  }
  //modal for request no of employee 
  openRequestEmployee(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {

    }, (reason) => { this.requestEmployeeForm.reset({ count: "" }); });
  }
//modal for facebook
  openFacebookModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
      this.fbService.getPageReviews(this.linkedFacebookPage);
    }, (reason) => { });
  }
//modal for google
  openGoogleModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
      // this.connectedWithGoogle = true;
      this.googleService.getLocationReviews(this.linkedGoogleAccountLocation);
    }, (reason) => { });
  }
//modal for zomato
  openZomatoModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
      // this.connectedWithZomato = true;
      this.zomatoService.getRestaurantReviews();
    }, (reason) => { });
  }

  get z() { return this.zomatoRestaurantForm.controls; }

  connectWithZomato() {
    this.zomatoService.getRestaurantDetails(this.zomatoRestaurantForm.value);
  }

  connectWithFacebook() {
    this.fbService.loginWithOptions();
  }

  selectFacebookPage(page: any) {
    this.linkedFacebookPage = page;
  }

  connectWithGoogle() {
    this.googleService.signInWithOfflineAccess();
  }

  selectGoogleAccount(account: any) {
    this.linkedGoogleAccount = account;
    this.googleService.getGoogleAccountLocations(this.linkedGoogleAccount);
  }

  selectGoogleAccountLocation(location: any) {
    this.linkedGoogleAccountLocation = location;
  }

  get c() { return this.changePasswordForm.controls; }
  get p() { return this.profileForm.controls; }
  get g() { return this.setgoalsForm.controls; }

  /**
   * 
   * @param platform : Holds the platform name
   * this function disconnect the perticuler platform
   */
  disconectPlatform(platform) {
    this.http.delete(this.baseUrl + '/user/disconnectplatform/' + this.user.data.email + '/' + platform)
      .subscribe(res => {
        // //////console.log("res" , res)
        if (res['success']) {

          if (platform == 'facebook') {
            this.alertService.success('Platform Disconnected Successfully !', true);
            this.fbService.init();
            this.fbService.connectedWithFacebook = false;
          } else if (platform == 'google') {
            this.alertService.success('Platform Disconnected Successfully !', true);
            this.googleService.init();
            this.googleService.connectedWithGoogle = false;
          } else {
            this.alertService.success('Platform Disconnected Successfully !', true);
            this.zomatoService.init();
            this.zomatoService.connectedWithZomato = false;
          }
        } else {
          // this.alertService.error('Something Went Wrong!', false);
        }
      });
  }
/**
 * changePassword() : this function is used for changing the current
 *  password of the user.
 */
  changePassword() {
    //////console.log("in change password")
    this.submittedPassword = true;
    if (this.changePasswordForm.invalid) {
      return;
    }
    if (this.c.newPassword.value !== this.c.confirmPassword.value) {
      this.alertService.error('New Password & Confirm Password does not match !', false);
      return;
    }
    this.changePasswordForm.patchValue({ email: this.user.data.email });
    //console.log("Form value : ", this.changePasswordForm.value);
    this.http.put(this.baseUrl + '/user/changepassword', this.changePasswordForm.value)
      .subscribe(res => {
        if (res['success']) {
          this.alertService.success('Password changed successfully!', false);
          this.changePasswordForm.reset({
            email: "",
            password: "",
            newPassword: "",
            confirmPassword: ""
          });
          this.submitted = false;
        } else {
          this.alertService.error("Current Password doesn't match!", false);
        }
      }, error => {
        // this.alertService.error('Error', false);
      });
  }

  /**
   * This function is for set the goals of perticular user
   */
  setGoals() {

    if (this.setgoalsForm.invalid) {
      return;
    }
    //////console.log("invitation goal post data : ", this.setgoalsForm.value)
    this.http.post(this.baseUrl + '/invitation/post', this.setgoalsForm.value)
      .subscribe(res => {
        //////console.log("res set goals : ", res);
        if (res['success']) {
          this.alertService.success('Goal/Invitation set successfully!', false);
          this.setgoalsForm.reset({ goal: "" });
          this.setgoalsForm.reset({ invitation: "" });
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
  }

/**
 * getEmployees() : get the list employees of perticular user
 */
  getEmployees() {
    this.http.get(this.baseUrl + '/user/getEmployees/' + this.user.data.email)
      .subscribe(res => {
        this.employees = res['data']
      });
  }

/**
 * 
 * @param email : Holds email id
 * getgoals() :  This function is used for get the goals of perticular
 * user by email id
 */
  getgoals(email: any) {
    this.http.get(this.baseUrl + '/invitation/get/' + email)
      .subscribe(data => {
        //////console.log("get goals :", data)
        if (data['success']) {
          this.setgoalsForm.patchValue({
            goal: data['data'].goal,
            invitation: data['data'].invitation
          });
        }
      });
  }

  //profile functions
  editProfile() {
    this.editMode = true;
    this.p.firstname.enable();
    this.p.lastname.enable();
  }
  // reset the some user details
  reset() {
    this.editMode = false;
    this.p.firstname.disable();
    this.p.lastname.disable();
    this.profileForm.reset({
      firstname: this.user.data.firstname,
      lastname: this.user.data.lastname,
      email: this.user.data.email,
      contactno: this.user.data.contactno,
      businessname: this.user.data.businessname
    });
  }
  /**
   * updateProfile(): This function is for update the user profile
   */
  updateProfile() {
    this.submittedProfile = true;
    if (this.profileForm.invalid) {
      return;
    }
    this.p.email.enable();
    // post updated user data...
    this.http.put(this.baseUrl + '/user/updateuser', this.profileForm.value)
      .subscribe(res => {
        if (res['success']) {
          this.submittedProfile = false;
          this.editMode = false;
          this.p.firstname.disable();
          this.p.lastname.disable();
          this.p.email.disable();
          this.alertService.success('Profile Updated Successfully!', false);
        } else {
          this.p.email.disable();
          this.alertService.error('Something went wrong!', false);
        }
      }, error => {
        this.alertService.error('Error!', false);
      });

  }

  // feedbackForm  with nested options
  feedbackform() {
    this.feedbackForm = this.fb.group({
      questionText: ['', [Validators.required, Validators.minLength(4)]],
      selectOption: [''],
      option: ['']
    })
  }
  get f() {
    return this.feedbackForm.controls;
  }

  //submition of feedback form
  onSubmitFeedbackForm() {
    this.submittedFeedback = true;
    var temp = this.feedbackForm.value
    this.questions.push(temp)
    let data = {
      email: this.user.data.email,
      feedbackFormData: this.questions
    }
    this.feedbackformService.addFeedbackForm(data).subscribe(res => {
      if (res['success']) {
        this.alertService.success('feedbackForm added successfully!', false);
        this.feedbackForm.reset();
        window.location.reload();
        this.submitted = false;
      }
    })
    // }
  }

//add options to feedback form
  addOption(): void {
    if (this.feedbackForm.get('option').value == '') {
      return;
    } else {
      this.radioOption.push({
        title: this.feedbackForm.get('option').value
      });
    }
    this.feedbackForm.get('option').reset();
  }
  remove(index) {
    this.radioOption.splice(index, 1);
  }
  test(key) {
    if (this.feedbackForm.get('option').value == '') {
      return;
    } else {
      if (key.keyCode == 13) {
        this.addOption();
      }
    }
  }


  getOptionFormControls(): AbstractControl[] {
    return (<FormArray>this.feedbackForm.get('options')).controls
  }

  addQuestion() {
    // this.feedbackForm = this.fb.group({
    //   questionText: ['', [Validators.required, Validators.minLength(4)]],
    //   selectOption: ['', Validators.required],
    //   options: ['']
    // })
    var obj = {
      questionText: this.feedbackForm.get('questionText').value,
      selectOption: this.feedbackForm.get('selectOption').value,
      options: this.radioOption.length > 0 ? this.radioOption : [],
      controlname: this.feedbackForm.get('questionText').value.split(/[ ,?]+/).join('_')
    }
    // var temp = this.feedbackForm.value    
    this.questions.push(obj)
    this.isoption = false;
    ////console.log(this.questions)
    this.feedbackForm.reset()
    this.radioOption = [];
  }

  onSelectOption(option) {
    if (option != "Radio") {
      this.isoption = true
    } else {
      this.isoption = false
    }
  }

  openDeletePlatformModal(content, platform) {
    this.platformname = platform
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

    })
  }
  /**
   * deleteplatform() : this function is used for delete perticular platform 
   */
  deleteplatform() {
    // //console.log("platform to delete : ", this.platformname)
    let data = {
      platform: this.platformname,
      email: this.user.data.email
    }
    this.userService.deleteplatform(data).subscribe(res => {
      // //console.log("res : ", res)
      if (res['success']) {
        this.alertService.success('Platform deleted successfully', true)
        this.modalService.dismissAll();
        if (this.platformname == 'google') {
          this.google = false;
        } else if (this.platformname == 'zomato') {
          this.zomato = false;
        } else {
          this.facebook = false;
        }
      }
    })
  }
  /**
   * request of platform
   */
  onRequestplatform(platformname) {

    let data = {
      email: this.user.data.email,
      platformname: platformname
    }
    // //console.log("data : ", data);

    this.userService.requestplatform(data).subscribe(res => {
      // //console.log("request res : ", res)
      if (res['success']) {
        this.alertService.success('Your request to add platform is sent successfully!', false);
        this.getplatformwithalldetails()
      }
    })
  }
  /**
   * request of Employees
   */
  onRequestEmpolyees() {
    this.requestEmployeeForm.patchValue({ email: this.user.data.email });
    //////console.log("this.requestEmployeeForm.value : ", this.requestEmployeeForm.value)
    this.userService.requestEmployees(this.requestEmployeeForm.value).subscribe(res => {
      // //console.log("res post request emp : ", res);
      if (res['success']) {
        this.alertService.success('Your request to add employees is sent successfully!', false);
        this.employeeRequestStatus = false;
        this.modalService.dismissAll();
      }

    })
  }
  /**
   * getnoOfEmployees() : This function is used to get no of employees of user
   */
  getnoOfEmployees() {
    this.userService.getnoOfEmployees().subscribe(res => {
      // //////console.log("res get res['data'].noOfEmplyees :", res['data'].noOfEmployees);
      if (res['success']) {
        let temp = res['data']
        this.noOfEmployees = temp.noOfEmployees;
        //////console.log(temp.noOfEmployees)
      }
    })
  }

  /**
  * function to get status of Employee credit request ,
  * set msgCreditRequestStatus variable to restrict user
  */
  getStatusOfEmployeeRequest() {
    this.userService.getStatusOfEmployeeRequest().subscribe(res => {
      //////console.log("getStatusOfEmployeeRequest res", res);
      if (res['success']) {
        this.employeeRequestStatus = res['data'].status
        //////console.log("status : ", this.employeeRequestStatus);
      }
    })
  }
  /**
  * function to get status of sms service credit request ,
  * set msgCreditRequestStatus variable to restrict user
  */
  getStatusOfSmsRequest() {
    this.userService.getStatusOfSmsRequest().subscribe(res => {
      console.log("getStatusOfSmsRequest :", res);
      if (res['success']) {
        console.log(this.feedbackTemplate)
        if (this.feedbackTemplate.length == 0) {
          this.smsRequestStatus = false;
          this.userService.sms = false;
        } else {
          this.smsRequestStatus = res['data'].status
          this.userService.sms = res['data'].status;
        }
      }
    })
  }
  /**
   * function to get status of msg credit request ,
   * set msgCreditRequestStatus variable to restrict user
   */
  getStatusMsgCreditRequest() {
    this.userService.getStatusMsgCreditRequest().subscribe(res => {
      ////console.log("getStatusMsgCreditRequest :", res);
      if (res['success']) {
        this.msgCreditRequestStatus = res['data'].status
        ////console.log("status : ", this.msgCreditRequestStatus);
      }
    })
  }

  feedbackTemplate: any = [];
  /**
   * getFeedbackForm() : This function is used for getting feedback form by
   * email id
   */
  getFeedbackForm() {
    this.http.get(this.baseUrl + '/feedbackform/getonefeedbackform/' + this.user.data.email).subscribe((res: any) => {
      if (res.success) {
        this.formId = res.data._id;
        this.feedbackTemplate = res.data.feedbackFormData.filter(res => {
          return res.questionText != null
        });
        this.userService.template = this.feedbackTemplate.length;
      } else {
        this.feedbackTemplate = [];
        this.userService.template = this.feedbackTemplate.length;
      }
    })
  }

  //modal for delete feedback
  openDeleteFeedBackModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

    })
  }
  modalDismiss() {
    this.modalService.dismissAll()
  }
  /**
   * deleteOneFeedbackForm() : This function is for delete one feedback form
   */
  deleteOneFeedbackForm() {
    this.feedbackformService.deleteOneFeedbackForm(this.formId).subscribe(res => {
      if (['success']) {
        this.alertService.success('feedbackForm deleted successfully!', false);
        this.modalService.dismissAll()
        this.getFeedbackForm();
        this.getusersSmsservicestatus();
        this.sharedService.getOneFeedbackForm(this.user.data.email)
      }
    })
  }
}