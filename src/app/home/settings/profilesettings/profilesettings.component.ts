import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profilesettings',
  templateUrl: './profilesettings.component.html',
  styleUrls: ['./profilesettings.component.css']
})
export class ProfilesettingsComponent implements OnInit {

  baseUrl: string = environment.baseUrl;
  user: any;
  
  submittedProfile: boolean = false;
  profileForm: FormGroup;
  profileData: any;
  editMode: boolean = false;

  constructor(
    private http: HttpClient,
    private alertService: AlertService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.profileForm = new FormGroup({
      firstname: new FormControl({ value: this.user.data.firstname, disabled: true }, Validators.required),
      lastname: new FormControl({ value: this.user.data.lastname, disabled: true }, Validators.required),
      email: new FormControl({ value: this.user.data.email, disabled: true }, Validators.required),
      contactno: new FormControl({ value: this.user.data.contactno, disabled: true }, Validators.required),
      businessname: new FormControl({ value: this.user.data.businessname, disabled: true }, Validators.required)
    });
  }

  get p() { return this.profileForm.controls; }

  editProfile() {
    this.editMode = true;
    this.p.firstname.enable();
    this.p.lastname.enable();
  }

  reset() {
    this.editMode = false;
    this.p.firstname.disable();
    this.p.lastname.disable();
    this.profileForm.reset({
      firstname: this.user.data.firstname,
      lastname: this.user.data.lastname,
      email: this.user.data.email,
      contactno: this.user.data.contactno,
      businessname: this.user.data.businessname
    });
  }

  updateProfile() {
    this.submittedProfile = true;
    if (this.profileForm.invalid) {
      return;
    }
    this.p.email.enable();
    // console.log(this.profileForm.value);
    // post updated user data...
    this.http.put(this.baseUrl + '/user/updateuser', this.profileForm.value)
      .subscribe(res => {
        if (res['success']) {
          this.submittedProfile = false;
          this.editMode = false;
          this.p.firstname.disable();
          this.p.lastname.disable();
          this.p.email.disable();
          this.alertService.success('Profile Updated Successfully!', false);
        } else {
          this.p.email.disable();
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });

  }
}
