import { Component, OnInit, ViewChild } from '@angular/core';
import { FbService } from 'src/app/services/fb.service';
import { GoogleService } from 'src/app/services/google.service';
import { ZomatoService } from 'src/app/services/zomato.service';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { AnalyticService } from '../../services/analytic.service';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { BaseChartDirective, Label } from 'ng2-charts';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/services/alert.service';
import { DatePipe } from '@angular/common'
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;
@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  //datepicker
  isShowDatePicker: boolean = false;
  model: NgbDateStruct;
  startDate: any = {};
  endDate: any = {};
  optionChoose: any;

  baseUrl: string = environment.baseUrl;
  year: number = 0;
  sevendays: number = 0;
  fifteendays; number = 0;
  googleData = [];
  lineChartLabels: Label[] = []
  
  // line chart options for year
  public lineChartLabelsForYear: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septmber', 'October', 'November', 'December'];
  public lineChartOptions: (ChartOptions & { annotation?: any }) = {
    // responsive: true,
    elements: {
      line: {
        tension: 0, // disables bezier curves,
        fill: false
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Months'
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            display: true,
            labelString: 'Reviews Count'
          }
          //,
          // ticks : {
          //   min : 0,
          //   max : 10
          // }
        }
      ]
    }
  };
  // line chart options for days
  public lineChartOptionsForDays: (ChartOptions & { annotation?: any }) = {
    // responsive: true,
    elements: {
      line: {
        tension: 0, // disables bezier curves,
        fill: false
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Days'
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            display: true,
            labelString: 'Reviews Count'
          }
        }
      ]
    }
  };
  public lineChartType = 'line';
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  public lineChartData: ChartDataSets[] = [
    { data: [], label: '' }
  ];
  public lineChartDataForYear: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: '' }
  ];
  username: string;
  user: any = {};
  emailid = ''
  constructor(
    config: NgbRatingConfig,
    public fbService: FbService,
    public googleService: GoogleService,
    public zomatoService: ZomatoService,
    public analyticService: AnalyticService,
    public http: HttpClient,
    public router: Router,
    private calendar: NgbCalendar,
    public alertService: AlertService,
    public datepipe: DatePipe,
    public userservice: UserService,
    public sharedservice : SharedService
  ) {
    //user restriction 
    //@auther vaishali
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.invalidUserStatus()
    
    if (this.user.data.role == "emp") {
      this.emailid = this.user.data.adminemail;
    } else {
      this.emailid = this.user.data.email;
    }
    // customize default values of ratings used by this component tree
    this.year = new Date().getFullYear();
    this.sevendays = 7;
    this.fifteendays = 15;
    config.max = 5;
    config.readonly = true;
    this.username = JSON.parse(localStorage.getItem('currentUser')).data.email;
    this.lineChartData = this.lineChartDataForYear;
    this.lineChartLabels = this.lineChartLabelsForYear;
    this.lineChartData = this.lineChartDataForYear;
    this.lineChartType = 'line';
    this.analyticData(this.year); 

    this.getusersSmsservicestatus();
    this.sharedservice.getOneFeedbackForm(this.emailid);    
    if(!this.sharedservice.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  analyticData(year) {
    this.analyticService.googleAnalytic(this.emailid, year).subscribe((res: any) => {
      // console.log("responce year : ", res);
      var data = res.data.slice(0);
      this.lineChartData = data.map(element => {
        return {
          data: element.data,
          label: element.platform
        }
      });
    })
  }
  ngOnInit() {

  }

  previousYear(year) {
    this.year = year - 1;
    this.lineChartData = this.lineChartDataForYear;
    this.lineChartLabels = this.lineChartLabelsForYear
    this.analyticData(this.year);
  }
  nextYear(year) {
    this.year = year + 1;
    this.lineChartData = this.lineChartDataForYear;
    this.lineChartLabels = this.lineChartLabelsForYear;
    this.analyticData(this.year);
  }

  lastdayslist(count) {
    var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    var goBackDays = count;
    var today = new Date(new Date().setDate(new Date().getDate()));
    var daysSorted = [];

    for (var i = 0; i < goBackDays; i++) {
      console.log(new Date(today.setDate(today.getDate())));
      var newDate = new Date(today.setDate(today.getDate() - 1));
      // console.log(days[newDate.getDay()]);
      // daysSorted.push(days[newDate.getDay()]);
      daysSorted.push(this.datepipe.transform(newDate, 'yyyy-MM-dd'));
    }
    // console.log(daysSorted);
    return daysSorted;

  }

  /**
   * user authentication : to check user status valid or not
   */
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.emailid).subscribe(res => {
      // console.log("res : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }
  /**
   * 
   * @param data : holds chart data
   * function for chart using days
   */
  chartByDays(data) {
    // console.log('data >>>', data.target.value);
    this.optionChoose = data.target.value;
    if (this.optionChoose == '7' || this.optionChoose == '15' || this.optionChoose == '30') {
      this.isShowDatePicker = false;
    } else if (this.optionChoose == 'dtrange') {
      this.isShowDatePicker = true;
    } else {
      this.isShowDatePicker = false;
    }
  }
  getLabelofChart(day): ChartDataSets[] {
    var arr = [];
    for (let i = 0; i < day; i++) {
      arr.push(0);
    }
    return [{
      data: arr,
      label: ''
    }]
  }
  /**
   * refreshing analytical data
   */
  refreshAnalyticData() {
    // console.log(typeof this.optionChoose)    
    if (this.optionChoose == '7' || this.optionChoose == '15' || this.optionChoose == '30') {
      this.lineChartData = this.getLabelofChart(this.optionChoose);
      this.lineChartLabels = this.lastdayslist(this.optionChoose);
      this.lineChartOptions = this.lineChartOptionsForDays;
      this.analyticService.getanalyticdatabydays(this.emailid, this.optionChoose).subscribe((res: any) => {
        // console.log("res refreshAnalyticData:" ,res)
        this.isShowDatePicker = false;
        var data = res.data.slice(0);
        this.lineChartData = data.map(element => {
          return {
            data: element.data,
            label: element.platform
          }
        });
      });
    } else if (this.optionChoose == 'dtrange') {
      this.isShowDatePicker = true;
      if (this.startDate.day > this.endDate.day) {
        this.alertService.warning('Please Check Valid Date', false);
      } else {
        // console.log(this.startDate,this.endDate)
        var startdate = new Date(this.startDate.year, this.startDate.month, this.startDate.day);
        var enddate = new Date(this.endDate.year, this.endDate.month, this.endDate.day);
        const diffTime = Math.abs(startdate.getTime() - enddate.getTime());
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        this.lineChartData = this.getLabelofChart(diffDays);
        this.lineChartLabels = this.lastdayslist(diffDays);
        this.lineChartOptions = this.lineChartOptionsForDays;
        this.analyticService.getanalyticdatabydays(this.emailid, diffDays).subscribe((res: any) => {
          // console.log("refreshAnalyticData 2 :", res)
          this.isShowDatePicker = true;
          var data = res.data.slice(0);
          this.lineChartData = data.map(element => {
            return {
              data: element.data,
              label: element.platform
            }
          });
        });
      }
    }
  }
  /**
   * For getting status of sms service
   */
  getusersSmsservicestatus() {
    let emailid;
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // console.log("res  : ", res);
      if (res["success"]) {
        this.userservice.sms = res['data'].smscampaining
        console.log("smscampaining : ", this.userservice.sms)
      }
    })
  }
}
