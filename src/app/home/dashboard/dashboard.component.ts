import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { FbService } from 'src/app/services/fb.service';
import { GoogleService } from 'src/app/services/google.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ZomatoService } from 'src/app/services/zomato.service';
import { FileUploader } from 'ng2-file-upload';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: any;
  role: string;
  roleEmp: boolean = false;
  baseUrl: string = environment.baseUrl;
  goalCount: number = 10;
  invitationCount: number = 0;
  invitation: number = 10;
  feedbackCountThisMonth: number;
  googleReplyForm: FormGroup;
  googleReview: any[] = [];
  sentinvitations: number = 0;
  daysLeft: number = 0;
  emailId: any;
  constructor(
    config: NgbRatingConfig,
    public modalService: NgbModal,
    public fbService: FbService,
    public googleService: GoogleService,
    public zomatoService: ZomatoService,
    public alertService: AlertService,
    private http: HttpClient,
    private router: Router,
    private userservice: UserService,
    private sharedservice : SharedService) {
    // customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = true;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.getGoalsCount();
    this.getFeedbackOfThisMonth();
    this.getinvitationCount();
    this.daysleft();
    this.googleReview = googleService.recentReviews;
    let email = '';
    if (this.user.data.role == "emp") {
      email = this.user.data.adminemail;
    } else {
      email = this.user.data.email;
    }
    this.getGoogleReview(email)
    this.getFaceBookReview(email)

    this.sharedservice.getOneFeedbackForm(email);        
    if(!this.sharedservice.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }   
  }
  ngOnInit() {
    this.invalidUserStatus();
    this.googleReview = this.googleService.recentReviews;
    this.googleReplyForm = new FormGroup({
      comment: new FormControl('', Validators.required)
    });
    this.role = this.user.data.role;
    if (this.role == 'emp') {
      this.roleEmp = true;
      this.emailId = this.user.data.adminemail;
    } else {
      this.roleEmp = false;
      this.emailId = this.user.data.email;
    }

  }

  divideBy() {
    if (this.fbService.connectedWithFacebook && this.zomatoService.connectedWithZomato && this.googleService.connectedWithGoogle) {
      return 3;
    } else if ((this.fbService.connectedWithFacebook && this.googleService.connectedWithGoogle) ||
      (this.fbService.connectedWithFacebook && this.zomatoService.connectedWithZomato) ||
      (this.googleService.connectedWithGoogle && this.zomatoService.connectedWithZomato)) {
      return 2;
    }
    return 1;
  }
/**
 * 
 * @param starRating : variable for star rating 
 * this function returns star rating in number format
 */
  getStarRatingValue(starRating: string): number {
    if (starRating == "FIVE") {
      return 5;
    } else if (starRating == "FOUR") {
      return 4;
    } else if (starRating == "THREE") {
      return 3;
    } else if (starRating == "TWO") {
      return 2;
    } else if (starRating == "ONE") {
      return 1;
    } else if (starRating == "positive") {
      return 5;
    }
    return 0;
  }

  /**
   * 
   * @param num : num variable holds number which we want to make round
   */
  round(num: any) {
    return Math.round(num);
  }
/**
 * 
 * @param review_name : holds reviewname
 * postGoogleReply() : This function is for posting reply on google
 */
  postGoogleReply(review_name: string) {
    if (this.googleReplyForm.invalid) {
      this.alertService.warning('Comment required!', false);
      return;
    }
    this.googleService.postReply(review_name, this.googleReplyForm.value);
    this.googleReplyForm.reset({ message: "" });
  }

  /**
   * This function is for getting goal count of perticular user or emp
   * using by email id
   */
  getGoalsCount() {
    if (this.user.data.role == 'emp') {
      this.http.get(this.baseUrl + '/invitation/get/' + this.user.data.email)
        .subscribe(res => {
          if (res['success']) {
            this.goalCount = res['data']['goal'];
            this.invitation = res['data']['invitation'];
            this.sentinvitations = res['data']['quick_invite_count'];
          } else {
            this.goalCount = 10;
            this.invitation = 10;
          }
        }, error => {
          // this.alertService.error('Error!', false);
        });
    } else {
      // //console.log(this.user.data.email);
      this.http.get(this.baseUrl + '/invitation/getGoalsAndInvitationsAdmin/' + this.user.data.email)
        .subscribe(res => {
          // //console.log(res);
          if (res['success']) {
            this.goalCount = res['data']['goal'];
            this.invitation = res['data']['invitation'];
            this.sentinvitations = res['data']['quick_invite_count'];
          } else {
            this.goalCount = 10;
            this.invitation = 10;
          }
        }, error => {
          // this.alertService.error('Error!', false);
        });
    }
  }

  /**
   * this function is for getting invitation count of perticular user
   */
  getinvitationCount() {
    // this.invitationCount = 0;
    this.http.get(this.baseUrl + '/invitation/count/' + this.user.data.email)
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.invitationCount = res['data']['quick_invite_count'];
        } else {
          // this.alertService.error("Something went wrong!", false);
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
  }

  /**
   * This function is for getting feedback of current month of perticular user/emp
   * by email id.
   */
  getFeedbackOfThisMonth() {
    if (this.user.data.role == 'emp') {
      this.http.get(this.baseUrl + '/feedback/getReviewsCountThisMonth/' + this.user.data.adminemail)
        .subscribe(res => {
          //console.log("count reviews CountThisMonth : ", res['reviewsCount'])
          if (res['success']) {
            this.feedbackCountThisMonth = res['reviewsCount'];
          }
        })
    } else {
      this.http.get(this.baseUrl + '/feedback/getReviewsCountThisMonth/' + this.user.data.email)
        .subscribe(res => {
          // //console.log("count reviews CountThisMonth : ", res['reviewsCount'])
          if (res['success']) {
            this.feedbackCountThisMonth = res['reviewsCount'];
          }
        })
    }

  }

  convertUnixToDate(date: any) {
    return new Date(date * 1000);
  }

  /**
   * daysleft(): This function is for getting days which are left to complete the 
   * goal.
   */
  daysleft() {
    this.userservice.daysleft(this.user.data.email).subscribe(data => {
      // //console.log("days left : " , data);
      this.daysLeft = data['data']
    })
  }

  /**
   * invalidUserStatus() : This function is for checking the status of user is active
   * or not. if active then continue else redirect to login page.
   */
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.user.data.email).subscribe(res => {
      // console.log("res : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }
  /**
   * getusersSmsservicestatus() : This function checks status of sms service of user
   */
  getusersSmsservicestatus() {
    let emailid;
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        this.userservice.sms = res['data'].smscampaining
      }
    })
  }
  _reviewGoogle: any = [];
  _reviewFacebook : any = [];

  /**
   * 
   * @param userName : this holds the username
   * getGoogleReview() : This function is for getting the google review by username
   */
  getGoogleReview(userName: string) {
    // //console.log('userName   ',userName)
    this.googleService.getGoogleReview(userName).subscribe((review: any) => {      
      this._reviewGoogle = {
        reviews : review.data.reviews.slice(0,2)
      }
      // //console.log('this.tempgoogleobj  ', this._reviewGoogle)
    });
  }
  /**
   * 
   * @param userName : this holds username
   * getFaceBookReview() : This function is for getting facebook reviews by username
   */
  getFaceBookReview(userName: string) {
    this.fbService.getFaceBookReview(userName).subscribe((review: any) => {                    
      var reviews : any= {
        review : review.data[0].reviews.slice(0,2)
      }      
      this._reviewFacebook = reviews
    });
  }

}
