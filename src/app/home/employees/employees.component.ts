import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

interface Employee {
  id?: number;
  firstname: string;
  lastname: string;
  dialCode?: string;
  contactno: number;
  email: string;
  selected?: boolean;
}
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  searchtext: any = '';
  submitted: boolean = false;
  submittedupdate: boolean = false;
  EMPLOYEES: Employee[] = [];
  p = 1;                     // not needed
  page: any;
  itemsPerPage = 10;
  pageSize = 10;
  collectionSize = this.EMPLOYEES.length;
  isAll: boolean = false;
  selectedEmployees: Employee[] = [];

  user: any = {};
  baseUrl: string = environment.baseUrl;
  addEmployeeForm: FormGroup;
  updateEmpForm: FormGroup;
  employeeEmail: string
  // employees : any;

  invitationgoal: any = 0;
  goal: any = 0;
  sentinvitations: any = 0;

  employeeCount: number;      //count of emp added
  noofemployee: number;       //setted no of emp 
  emplimitfull: boolean = false;
  // leftemployees : number;
  constructor(
    public modalService: NgbModal,
    public http: HttpClient,
    public alertService: AlertService,
    public userService: UserService,
    public sharedService : SharedService
  ) {
    this.getusersSmsservicestatus();
    this.user = JSON.parse(localStorage.getItem('currentUser'));

    var emailid = '';
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }    
    this.sharedService.getOneFeedbackForm(emailid);    
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  get emp() { return this.addEmployeeForm.controls; }
  get empu() { return this.updateEmpForm.controls; }
  ngOnInit() {

    //add employee form
    this.addEmployeeForm = new FormGroup({
      firstname: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
      lastname: new FormControl('', [Validators.pattern(/^[a-zA-Z ]+$/)]),
      contactno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      address: new FormControl(''),
      businessname: new FormControl(''),
      role: new FormControl(''),
      adminemail: new FormControl(''),
      dialCode: new FormControl('91'),
    });

    //update employee form
    this.updateEmpForm = new FormGroup({
      // 
      firstname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
      lastname: new FormControl('', [Validators.pattern(/^[a-zA-Z ]+$/)]),
      // contactno: new FormControl('', [Validators.required, Validators.pattern(/^(\+\d{1,3}[- ]?)?\d{10}$/)]),
      contactno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
      email: new FormControl('', Validators.email),
    });
    this.getEmployees();
    this.getEmployeeCount();
    // this.getnoOfEmployee();
    // this.checkEmpCount();
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.submitted = false;
      this.addEmployeeForm.reset({
        "firstname": "",
        "lastname": "",
        "contactno": "",
        "email": "",
        "password": "",
        "adminemail": "",
        "role": "",
        "address": "",
        "businessname": "",
      });
    });
  }
  openDeleteEmployeeModal(content, employee) {
    //console.log('cust=>', employee)
    this.employeeEmail = employee.email
    //console.log('cust=>', this.employeeEmail)
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

    })
  }
  onCountryChange(obj) {
    // //console.log(obj);
    this.addEmployeeForm.patchValue({ dialCode: obj.dialCode });
  }
  /**
   * function for adding new employee
   */
  addEmployee() {
    this.submitted = true;
    if (this.addEmployeeForm.invalid) {
      return;
    }
    this.addEmployeeForm.patchValue({
      role: 'emp',
      adminemail: this.user.data.email,
      businessname: this.user.data.businessname
    });

    // //console.log(this.addEmployeeForm.value);
    this.http.post(this.baseUrl + '/user/addEmployee', this.addEmployeeForm.value)
      .subscribe(res => {
        // //console.log("response from server : " , res);
        if (res['success']) {
          this.alertService.success('Employee added successfully!', false);
          this.modalService.dismissAll();
          this.getEmployees();
        } else {
          // this.alertService.error("Something went wrong!", false);
        }
      }, error => {
        // this.alertService.error("Error", false);
      });

    this.submitted = false;
    this.addEmployeeForm.reset({
      "firstname": "",
      "lastname": "",
      "contactno": "",
      "email": "",
      "password": "",
      "adminemail": "",
      "role": "",
      "address": "",
      "businessname": "",
    });
    // this.getEmployeeCount();
  }
  /**
   * getEmployeeCount() : for getting count of employees added under
   * perticular admin/user. This function get the total no of employees of admin and
   * also check for employee limit
   */
  getEmployeeCount() {
    this.userService.getEmployeeCount().subscribe(res => {
      if (res['success']) {
        this.employeeCount = res['data'];
        this.userService.getnoOfEmployees().subscribe(res => {
          if (res['success']) {
            this.noofemployee = res['data'].noOfEmployees;
            if (this.employeeCount == this.noofemployee) {
              // //console.log("limit full")
              this.emplimitfull = true;
            } else {
              this.emplimitfull = false;
            }
          }
        })

      }
    })
  }
  /**
   * getnoOfEmployee : Used for getting no .of emp set for admin
   */
  // getnoOfEmployee(){
  //   this.userService.getnoOfEmployees().subscribe(res=>{
  //     //console.log("getnoOfEmployee" , res['data'].noOfEmployees)
  //     if(res['success']){
  //       this.noofemployee = res['data'].noOfEmployees;
  //     }
  //   })
  //   this.checkEmpCount();
  // }
  // checkEmpCount(){
  //   if(this.employeeCount == this.noofemployee){
  //     //console.log("limit full")
  //       this.emplimitfull = true;
  //       //console.log("emplimitfull" , this.emplimitfull)
  //   }else{
  //     this.emplimitfull = false;
  //     // //console.log("rahilet");
  //   }
  // }

  /**
   * getEmployees() : This function is used for employees getting List of
   * employees and assign it to table 
   */
  getEmployees() {
    this.http.get(this.baseUrl + '/user/getEmployees/' + this.user.data.email)
      .subscribe(res => {
        //console.log(res);
        this.EMPLOYEES = res['data'];
        this.EMPLOYEES.map(res => {
          res.selected = false;
        });
        this.collectionSize = this.EMPLOYEES.length;
        //console.log('this.EMPLOYEES >>', this.EMPLOYEES)
      });
  }
/**
 * function for employee table with all relevant emp data 
 */
  get employees(): Employee[] {
    return this.EMPLOYEES
      .map((employee, i) => ({ id: i + 1, ...employee }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  //select one employee
  selectOne(index: number, status: boolean) {
    // //console.log(index, status);
    this.isAll = false;
    if (status) {
      this.EMPLOYEES[index].selected = false;
    } else {
      this.EMPLOYEES[index].selected = true;
    }
  }

  /**
   * updateEmployee() : This function is for update the perticular employee
   * if employee updated successfully then gives success message else gives warning
   */
  updateEmployee() {
    this.submittedupdate = true;
    if (this.updateEmpForm.invalid) {
      return;
    }
    this.http.put(this.baseUrl + '/user/updateEmployee', this.updateEmpForm.value)
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.modalService.dismissAll();
          this.alertService.success('Employee updated successfully!', true);
          this.getEmployees();
        } else {
          this.alertService.warning('Something went wrong', false);
        }
      });
  }

  /**
   * deleteEmployee() : this function is for delete the employee
   */
  deleteEmployee() {
    // if (confirm('Do you really want to delete employee?')) {
    this.http.delete(this.baseUrl + '/user/deleteemployee/' + this.employeeEmail)
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.alertService.success('Employee deleted successfully!', true);
          this.getEmployees();
          this.modalService.dismissAll()
          const ele = document.getElementById("selectAllCheckbox") as HTMLInputElement;
          ele.checked = false;
        } else {
          this.alertService.warning('Something went wrong', false);
        }
      });
    // }
  }
/**
 * 
 * @param content : modal content
 * @param data : data variable holds emplyee data
 * This is for open the update employee model with employee data
 * which allows user to update employee
 */
  openUpdateEmpModal(content, data: any) {
    // //console.log(data);
    this.updateEmpForm.patchValue(data);
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.submitted = false;
      this.updateEmpForm.reset({
        firstname: "",
        lastname: "",
        contactno: "",
        email: "",
        address: "",
      });
    });
  }
  /**
   * 
   * @param content : model content
   * this model is for delete 
   */
  openDeleteEmpModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

    })
  }
 /**
  * 
  * @param email : this varible holds email id
  * getGoalsCount(): This function is for getting 
  * goal count , invitation goal , sent invitation
  */
  getGoalsCount(email: any) {
    // if (this.user.data.role == 'user') {
    this.http.get(this.baseUrl + '/invitation/get/' + email)
      .subscribe(res => {
        // //console.log("res : ", res)
        if (res['success']) {
          this.goal = res['data']['goal'];
          this.invitationgoal = res['data']['invitation'];
          this.sentinvitations = res['data']['quick_invite_count'];
        } else {
          this.goal = 10;
          this.invitationgoal = 10;
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
    // } 
  }

  /**
   * 
   * @param content  : model content
   * @param email  : this variable holds email id
   * showEmpDetailsModal() : this model is for view epmloyee details
   * e.g name , phone no etc..
   */
  showEmpDetailsModal(content, email: any) {
    this.getGoalsCount(email);
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.submitted = false;
    });
  }

  alphaOnly(event) {
    var key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8);
  };
  absoluteIndex(indexOnPage: number): number {
    return this.itemsPerPage * (this.p - 1) + indexOnPage;
  }
  pageChanged(event) {
    this.p = event;
  }
  change(index: number) {
    var actualindex = this.absoluteIndex(index);
    this.EMPLOYEES.forEach((d, index) => {
      if (actualindex == index) {
        if (this.EMPLOYEES[actualindex].selected) {
          this.EMPLOYEES[actualindex].selected = false
        } else {
          this.EMPLOYEES[actualindex].selected = true
        }
      }
    })
    // //console.log()
  }
  selectAll(flag) {
    this.isAll = !flag;
    if (this.isAll) {
      this.EMPLOYEES.forEach((cust, index) => {
        this.EMPLOYEES[index].selected = true;
      });
    } else {
      this.EMPLOYEES.forEach((cust, index) => {
        this.EMPLOYEES[index].selected = false;
      });
    }
  }
  deleteSelected() {
    var emp = this.EMPLOYEES.filter(res => {
      //console.log('res.selected >>', res)
      return res.selected == true
    });
    //console.log('emp :::::',emp)
    this.http.post(this.baseUrl+"/user/deleteemployees",{
      employees : emp
    }).subscribe((res:any)=>{
      if(res.success){
        this.alertService.success("Employee deleted.")
        this.getEmployees();
        this.modalService.dismissAll();
      }
    })
  }
  deleteSelectedmodal(content) {
    //change
    var emp = this.EMPLOYEES.filter(res => {
      //console.log('res.selected >>', res)
      return res.selected == true
    });
    if(emp.length == 0){
      this.alertService.warning('Please Select at least one employee', false);
    }else{
      this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

      })
    }    
  }
  /**
   * getuserSmsservicestatus() : This function is getting 
   *  the status of sms service of user i .e pending or not
   */
  getusersSmsservicestatus() {
    let emailid;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    // //console.log(this.user)
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        this.userService.sms = res['data'].smscampaining;
        //console.log("smscampaining : ", this.userService.sms)
      }
    })
  }
}