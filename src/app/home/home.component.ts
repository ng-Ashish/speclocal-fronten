import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router, NavigationStart } from '@angular/router';
import { ZomatoService } from '../services/zomato.service';
import { GoogleService } from '../services/google.service';
import { FbService } from '../services/fb.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploader } from 'ng2-file-upload';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from '../services/alert.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from '../services/user.service';
import { SharedService } from '../services/shared.service';
declare var alertify: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: any;
  baseUrl: string = environment.baseUrl;
  activePath: string;
  submitted: boolean = false;
  role: string;
  roleEmp: boolean = false;
  @ViewChild('imageSelect') imageFile: ElementRef;
  image = null;
  hide = true;
  public uploader: FileUploader = new FileUploader({ url: "https://speclocal.com:4500/api/uploadimage", itemAlias: 'image' });
  quickPostForm: FormGroup;
  inviteForm: FormGroup;
  referFriendForm: FormGroup;
  emailid: any = {};
  today: any;
  sms: boolean;
  @ViewChild('submenu') submenu: ElementRef<HTMLElement>
  constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    public zomatoService: ZomatoService,
    public googleService: GoogleService,
    public fbService: FbService,
    public alertService: AlertService,
    public http: HttpClient,
    public modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    public userService: UserService,
    public sharedService: SharedService) {
    // clear alert message on route change
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.activePath = event.url;
      }
    });

    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == "emp") {
      this.emailid = this.user.data.adminemail;
    } else {
      this.emailid = this.user.data.email;
    }
    this.getusersSmsservicestatus();
    this.getCampaignTemlates();

    //invite form
    this.inviteForm = new FormGroup({
      email: new FormControl(this.emailid),
      employeeemail: new FormControl(this.user.data.email),
      client_id: new FormControl(this.user.data._id),
      type: new FormControl('quickinvite'),
      businessname: new FormControl(this.user.data.businessname),
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
      dialCode: new FormControl('91'),
      phoneno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)])
    });

    //quick post form
    this.quickPostForm = new FormGroup({
      onFacebook: new FormControl(false),
      onGoogle: new FormControl(false),
      message: new FormControl('', Validators.required),
      link: new FormControl(''),
      source: new FormControl(''),
      actionType: new FormControl('')
    });

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false };
    this.uploader.onCompleteItem = (item: any, res: any, status: any, headers: any) => {
      // const resp = JSON.parse(res);
      this.image = res;
      // console.log(this.image);
      this.quickPostForm.get('source').setValue(res);
    };

    this.formControlValueChanged();
    this.today = new Date();
    if (!this.sharedService.isFeedbakTemplate) {
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  // -----------------------------------------------------------------------------

  ngOnInit() {
    this.activePath = this.router.url;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.zomatoService.init();
    this.googleService.init();
    this.userService.init();
    this.fbService.init();
    this.role = this.user.data.role;
    if (this.role == 'emp') {
      this.roleEmp = true;
    } else {
      this.roleEmp = false;
    }
  }

  // -----------------------------------------------------------------------------

  //logout function for nevigate to login page
  logout() {
    this.modalService.dismissAll();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
  /**
   * function to clear the buffer image
   */
  removeImg() {
    this.image = '';
  }
  // -----------------------------------------------------------------------------
  onSubmit() {
    if (this.inviteForm.invalid) {
      return;
    }

    if (this.userService.msgcredits == 0) {
      // console.log(this.userService.msgcredits);
      console.log('invite :', this.inviteForm.value)

      this.alertService.warning("Can't invite! No SMS credits available.");
      this.referFriendForm.patchValue({ dialCode: '91' });
      this.inviteForm.reset({ name: "", dialCode: '91', phoneno: "", type: "quickinvite", businessname: this.user.data.businessname, email: this.emailid, employeeemail: this.user.data.email, client_id: this.user.data._id });
      this.modalService.dismissAll();
      return;
    }

    // console.log(this.inviteForm.value);
    // this.inviteForm.patchValue({
    //   phoneno: this.inviteForm.get('dialCode').value + this.inviteForm.get('phoneno').value
    // });
    // console.log(this.inviteForm.value)
    this.inviteForm.patchValue({
      dialCode: '91',
      type: 'quickinvite',
      businessname: this.user.data.businessname,
      email: this.emailid,
      employeeemail: this.user.data.email,
      client_id: this.user.data._id
    });
    this.http.post(this.baseUrl + '/invitation/invitationByMessage', this.inviteForm.value).subscribe(res => {
      //  console.log(res);
      if (res['success']) {
        this.alertService.success('Message Sent Successfully!');
        this.modalService.dismissAll();
        this.userService.getCredits();
      }
      else {
        // this.alertService.error('Something went wrong!', false);
      }
    }, error => {
      // this.alertService.error('Error!', false);
    });
    this.referFriendForm.patchValue({ dialCode: '91' });
    this.inviteForm.reset();
  }

  // -----------------------------------------------------------------------------
  /**
   * function for refferring the friend
   */
  refernow() {
    if (this.referFriendForm.invalid) {
      return;
    }
    if (this.userService.msgcredits == 0) {
      this.alertService.warning("Can't refer! No SMS credits available.");
      this.inviteForm.patchValue({ dialCode: '91' });
      this.referFriendForm.reset({ name: "", dialCode: '91', phoneno: "", type: "referfriend", businessname: this.user.data.businessname, email: this.user.data.email, client_id: this.user.data._id });
      this.referFriendForm.patchValue({
        refermessagebody: this.templates[0].messagebody
      });
      this.modalService.dismissAll();
      return;
    }
    // console.log(this.referFriendForm.value);
    // this.referFriendForm.patchValue({
    //   phoneno: this.referFriendForm.get('dialCode').value + this.referFriendForm.get('phoneno').value
    // });

    this.inviteForm.patchValue({
      dialCode: '91',
      type: 'referfriend',
      businessname: this.user.data.businessname,
      email: this.emailid,
      employeeemail: this.user.data.email,
      client_id: this.user.data._id
    });
    this.http.post(this.baseUrl + '/invitation/invitationByMessage', this.referFriendForm.value).subscribe(res => {
      // console.log(res);
      if (res['success']) {
        this.alertService.success('Message Sent Successfully!');
        // this.referFriendForm.reset({ name: "", phoneno: "", type: "referfriend", businessname: this.user.data.businessname });
        this.modalService.dismissAll();
        this.userService.getCredits();
      }
      else {
        // this.alertService.error('Something went wrong!', false);
        // this.referFriendForm.reset({ name: "", phoneno: "", type: "referfriend", businessname: this.user.data.businessname });
      }
    }, error => {
      // this.alertService.error('Error!', false);
      // this.referFriendForm.reset({ name: "", phoneno: "", type: "referfriend", businessname: this.user.data.businessname });
    });
    this.inviteForm.patchValue({ dialCode: '91' });
    this.referFriendForm.reset();
  }

  formControlValueChanged() {
    this.quickPostForm.get('actionType').valueChanges.subscribe(
      (mode: string) => {
        if (mode) {
          this.quickPostForm.get('link').setValidators([Validators.required]);
        } else {
          this.quickPostForm.get('link').clearValidators();
        }
        this.quickPostForm.get('link').updateValueAndValidity();
      });
  }

  //open modal for quick invite
  openQuickInviteModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {

    }, (reason) => {
      this.referFriendForm.patchValue({ dialCode: '91' });
      this.inviteForm.reset({ name: "", dialCode: '91', phoneno: "", type: "quickinvite", businessname: this.user.data.businessname, email: this.user.data.email, client_id: this.user.data._id });
    });
  }
  //open modal for reffer friend
  openReferFriendModal(content) {
    this.referFriendForm.patchValue({
      refermessagebody: this.templates[0].messagebody
    });
    this.modalService.open(content, { centered: true }).result.then((result) => {

    }, (reason) => {
      this.inviteForm.patchValue({ dialCode: '91' });
      this.referFriendForm.reset({ name: "", dialCode: '91', phoneno: "", type: "referfriend", businessname: this.user.data.businessname, email: this.user.data.email, client_id: this.user.data._id });

    });
  }

  //opens logout modal
  openLogoutModal(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  //open quick post modal
  openQuickPostModal(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.image = null;
      this.submitted = false;
      this.quickPostForm.reset({
        onFacebook: false,
        onGoogle: false,
        message: "",
        link: "",
        source: "",
        actionType: ""
      });
    });
  }

  get q() { return this.quickPostForm.controls; }

  post() {

    this.submitted = true;
    if (this.quickPostForm.invalid) {
      return;
    }
    if (!this.quickPostForm.get('onGoogle').value && !this.quickPostForm.get('onFacebook').value) {
      this.alertService.warning('Select Atleast One Platform To Post!', false);
      return;
    }
    // If post on Google is selected...
    if (this.quickPostForm.get('onGoogle').value == true && this.googleService.linked_location) {

      let req = {
        "languageCode": "en-US",
        "summary": this.quickPostForm.get('message').value,
        "callToAction": {
          "actionType": this.quickPostForm.get('actionType').value,
          "url": this.quickPostForm.get('link').value,
        },
        "media": [
          {
            "mediaFormat": "PHOTO",
            "sourceUrl": this.quickPostForm.get('source').value,
          }
        ]
      }
      if (this.quickPostForm.get('actionType').value == "") {
        delete req['callToAction'];
      }
      if (this.quickPostForm.get('actionType').value == "CALL") {
        delete req['callToAction']['url'];
      }
      if (this.quickPostForm.get('source').value == "") {
        delete req['media'];
      }
      this.googleService.quickPost(this.googleService.linked_location.name, req);
    }

    // If post on facebook is selected...
    if (this.quickPostForm.get('onFacebook').value === true && !this.fbService.linked_page) {
      // this.alertService.error("Can't Post, Not Connected with Facebook !", false);
      return;
    }

    if (this.quickPostForm.get('onFacebook').value === true && this.fbService.linked_page) {
      // console.log(this.quickPostForm.value);
      this.fbService.quickPost(this.fbService.linked_page, this.quickPostForm.value);
    }

    this.image = null;
    // this.imageFile.nativeElement.value = '';
    this.quickPostForm.reset({
      onFacebook: false,
      onGoogle: false,
      message: "",
      link: "",
      source: "",
      actionType: ""
    });
    this.submitted = false;
    this.alertService.success('Posted Successfully !', false);
  }
  //manage the country change event
  onCountryChange(obj) {
    // console.log(obj);
    this.inviteForm.patchValue({ dialCode: obj.dialCode });
    this.referFriendForm.patchValue({ dialCode: obj.dialCode });
  }
  /**
   * Function for get sms service status for perticular user
   * this ingo is get by using email id
   */
  getusersSmsservicestatus() {
    let emailid;
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // console.log("res  : ", res);
      if (res["success"]) {
        this.userService.sms = res['data'].smscampaining
        console.log("smscampaining : ", this.userService.sms)
      }
    })
  }
  templates: any = [];
  /**
   * This function is used to getting the campaign template
   */
  getCampaignTemlates() {
    let emailid = ''
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/templatemessage/gettemplatemessage/' + emailid)
      .subscribe((res: any) => {
        if (res.success) {
          this.templates = res.data.filter(template => {
            return template.templatename == 'Referral'
          });

          this.referFriendForm = new FormGroup({
            email: new FormControl(this.emailid),
            client_id: new FormControl(this.user.data._id),
            type: new FormControl('referfriend'),
            businessname: new FormControl(this.user.data.businessname),
            name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
            dialCode: new FormControl('91'),
            phoneno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
            refermessagebody: new FormControl(this.templates[0].messagebody, [Validators.required])
          });
        }

      });
  }
}

