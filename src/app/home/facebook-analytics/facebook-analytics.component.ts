import { Component, OnInit } from '@angular/core';
import { FbService } from 'src/app/services/fb.service';
import { FacebookService } from 'ngx-facebook';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

@Component({
  selector: 'app-facebook-analytics',
  templateUrl: './facebook-analytics.component.html',
  styleUrls: ['./facebook-analytics.component.css']
})
export class FacebookAnalyticsComponent implements OnInit {
  user: any = {};
  datePreset: string = 'last_7d'
  action_on_page: number = 0;
  page_views: number = 0;
  page_previews: number = 0;
  page_likes: number = 0;
  post_reach: number = 0;
  stoey_reach: number = 0;
  recommendation: number = 0;
  page_engagement: number = 0;
  videos: number = 0;
  total_follower : number = 0;
  email: string = ''
  constructor(public fbService: FbService, public FBAPI: FacebookService,public sharedService : SharedService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    if (this.user.data.role == "emp") {
      this.email = this.user.data.adminemail;
    } else {
      this.email = this.user.data.email;
    }
    this.insight(this.email, this.datePreset)        
    this.sharedService.getOneFeedbackForm(this.email);
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }

  insight(email, preset) {
    this.fbService.getfbinsight(email).subscribe((res: any) => {
      //console.log('response :;', res)
      if (res.connected_with_facebook) {
        var accessToken = res.data.linked_page.access_token;
        var id = res.data.linked_page.id;
        //total page views        
        //page_views_total
        //console.log('https://graph.facebook.com/v3.2/' + id + '/insights/page_views_total?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken)
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_views_total?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            // //console.log(res)
            if (res.data[0] == undefined) {
              this.page_views = 0;
            } else {
              res.data[0].values.forEach(element => {
                if (element.value == undefined) {
                  this.page_views = this.page_views + 0;
                } else {
                  this.page_views = this.page_views + element.value;
                }
              });
            }
            // //console.log('this.page_views <>', this.page_views);
          })

        //think later
        //page action
        //page_actions_post_reactions_like_total
        // this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_actions_post_reactions_like_total?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
        //   .then((res: any) => {
        //     if (res.data[0] == undefined) {
        //       this.page_engagement = 0;
        //     } else {
        //       res.data[0].values.forEach(element => {
        //         this.page_likes = this.page_likes + element.value
        //       });
        //     }
        //     // //console.log('this.page_likes <>', this.page_likes);
        //   })

        //post engagement
        //page_post_engagements
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_post_engagements?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            if (res.data[0] == undefined) {
              this.page_engagement = 0;
            } else {
              res.data[0].values.forEach(element => {
                this.page_engagement = this.page_engagement + element.value
              });
            }
            //console.log('this.page_engagement <>', this.page_engagement);
          })

        //action on page
        //page_total_actions
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_total_actions?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            if (res.data[0] == undefined) {
              this.action_on_page = 0;
            } else {
              res.data[0].values.forEach(element => {
                this.action_on_page = this.action_on_page + element.value
              });
            }
            // //console.log('this.action_on_page <>', this.action_on_page);
          })
        //page_actions_post_reactions_total
        //post reach
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_actions_post_reactions_total?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            // //console.log(res)
            if (res.data[0] == undefined) {
              this.post_reach = 0;
            } else {
              res.data[0].values.forEach(element => {
                // //console.log('element.value.like   ', element.value)
                if (element.value == undefined) {
                  this.post_reach = this.post_reach + 0;
                } else {
                  if (element.value.like != undefined) {
                    this.post_reach = this.post_reach + element.value.like;
                  }
                }
              });
            }
            // //console.log('this.post_reach <>', this.post_reach);
          })

        //video post watched
        //post_video_avg_time_watched
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/post_video_avg_time_watched?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            if (res.data[0] == undefined) {
              this.videos = 0;
            } else {
              res.data[0].values.forEach(element => {
                if (element.value == undefined) {
                  this.videos = this.videos + 0;
                } else {
                  this.videos = this.videos + element.values;
                }
              });
            }
            // //console.log('this.videos <>', this.videos);
          })

        //story reach
        //page_content_activity_by_action_type_unique
        this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '/insights/page_content_activity_by_action_type_unique?fields=description,id,description_from_api_doc,name,period,title,values&date_preset=' + preset + '&access_token=' + accessToken, 'get')
          .then((res: any) => {
            if (res.data[0] == undefined) {
              this.stoey_reach = 0;
            } else {
              res.data[0].values.forEach(element => {
                if (element.values == undefined) {
                  this.stoey_reach = this.stoey_reach + 0;
                } else {
                  this.stoey_reach = this.stoey_reach + element.values;
                }
              });
            }
            // //console.log('this.stoey_reach <>', this.stoey_reach);
          })
          
          //total follower
          this.FBAPI.api('https://graph.facebook.com/v3.2/' + id + '?fields=fan_count&access_token=' + accessToken, 'get')
          .then((res: any) => {           
            if(res.fan_count > 0) {
              this.page_likes = res.fan_count
            }else{
              this.page_likes = 0
            }
            //console.log('follower <>', res);
          })
      }
    })
  }
  chagePreset(preset) {
    this.action_on_page = 0;
    this.page_views = 0;
    this.page_previews = 0;
    this.page_likes = 0;
    this.post_reach = 0;
    this.stoey_reach = 0;
    this.recommendation = 0;
    this.page_engagement = 0;
    this.videos = 0;
    this.insight(this.email, preset.target.value)
  }
  ngOnInit() {

  }

}
