import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AlertService } from 'src/app/services/alert.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { FileSaverService } from 'ngx-filesaver';
import { FileUploader } from 'ng2-file-upload';
import { UserService } from 'src/app/services/user.service';
import { getListeners } from '@angular/core/src/render3/discovery_utils';
import { SharedService } from 'src/app/services/shared.service';
declare var alertify: any;

const URL = environment.baseUrl + '/api/upload';

/**
 * interface for import customers by csv file
 */
interface Importcustomer {
  id?: number;
  name: string;
  dialCode?: string;
  phoneno: number;
  email: string;
  occupation: string;
  birthdate: string;
  anniversary: string;
  address: string;
}
/**
 * Interface for create customer 
 */
interface Customer {
  id?: number;
  _id: string;
  name: string;
  dialCode?: string;
  phoneno: number;
  email: string;
  occupation: string;
  birthdate: string;
  anniversary: string;
  address: string;
  selected?: boolean;
  isSelected?: string;
}

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit, OnDestroy {

  searchtext: any = '';
  //paginator 
  p = 1;
  itemsPerPage = 10;
  page: any;
  isAllcustomer: boolean;
  //customer variables
  isAll: boolean = false;
  selectedCustomers: Customer[] = [];
  selectedCustomersCampaigned: string[] = [];
  selectedCustomersFeedbacked: string[] = [];
  CUSTOMERS: Customer[] = [];
  collectionSize = this.CUSTOMERS.length;
  updateCustomerForm: FormGroup;
  customerId: any

  //import customers
  IMPORTCUSTOMERS: Importcustomer[] = [];
  importpage = 1;
  importpageSize = 10;
  importcollectionSize = this.IMPORTCUSTOMERS.length;
  response: any;

  user: any;
  baseUrl: string = environment.baseUrl;
  addCustomerForm: FormGroup;
  submitted: boolean = false;
  filename: any = "Choose file";
  @ViewChild('navaddCustomerstab')
  navaddCustomerstab: ElementRef<HTMLElement>;
  validCsv: boolean = true;
  constructor(
    public modalService: NgbModal,
    public http: HttpClient,
    public alertService: AlertService,
    private ngxService: NgxUiLoaderService,
    public router: Router,
    public fileSaverService: FileSaverService,
    public userservice: UserService,
    public sharedService : SharedService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.invalidUserStatus()
    this.getusersSmsservicestatus();
    this.isAllcustomer = false;
    this.getCustomers();

    var emailid = '';
    if (this.user.data.role == "emp") {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }    
    this.sharedService.getOneFeedbackForm(emailid);    
    if(!this.sharedService.isFeedbakTemplate){
      alertify.alert('Feedback Form', 'Please Create Feedback Form Template to Enable Quick invite / Message Campaign.', function () { }).set({ transition: 'zoom' });
    }
  }
  ngOnDestroy() {
    this.isAllcustomer = false;
  }
  triggerFalseClick() {
    let el: HTMLElement = this.navaddCustomerstab.nativeElement;
    el.click();
  }
  check() {
    this.getCustomers();
  }
  /**
   * invalidUserStatus() : User is disabled by superAdmin for some reason
   * then this function check user is disabled or not , if user is disabled
   * then it redirects user to login page
   */
  invalidUserStatus() {
    this.http.get(this.baseUrl + '/user/getuserstatus/' + this.user.data.email).subscribe(res => {
      // //console.log("res : ", res);
      if (res["success"]) {
        if (!res['status']) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/', 'login']);
        }
      }
    });
  }

  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'file' });

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.uploader.onAfterAddingFile = (file) => {
      //console.log("file : ", file._file.name)
      this.filename = file._file.name;
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      let res = JSON.parse(response);  //response : only success or false      
      
      for (let i = 0; i < res.data.length; i++) {
        //console.log(res.data[i].name, res.data[i].phoneno, res.data[i].dialCode)
        if (res.data[i].name == '' || res.data[i].phoneno == '' || res.data[i].dialCode == '') {
          this.validCsv = false;
          break;
        }
      }
      //console.log('this.validCsv :::',this.validCsv)
      if (this.validCsv) {
        this.IMPORTCUSTOMERS = res.data;
        console.log('this.IMPORTCUSTOMERS    ',this.IMPORTCUSTOMERS)
        this.importcollectionSize = this.IMPORTCUSTOMERS.length;
      } else {
        alertify.alert('CSV Import', 'Please fill appropriate data in CSV', function () { }).set({ transition: 'zoom' });
      }
    };

    this.addCustomerForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
      dialCode: new FormControl('91'),
      phoneno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
      email: new FormControl('', Validators.email),
      occupation: new FormControl(''),
      birthdate: new FormControl(''),
      anniversary: new FormControl(''),
      address: new FormControl('')
    });

    this.updateCustomerForm = new FormGroup({
      _id: new FormControl('', Validators.required),
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
      dialCode: new FormControl('91'),
      phoneno: new FormControl('', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
      email: new FormControl('', Validators.email),
      occupation: new FormControl(''),
      birthdate: new FormControl(''),
      anniversary: new FormControl(''),
      address: new FormControl(''),
      campaignmsg: new FormControl(''),
      is_feedback: new FormControl('')
    });
    // this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  /**
   * getCustomers(): This function is used for gettting the customer list of 
   * perticular user or employee
   */
  getCustomers() {
    this.http.get(this.baseUrl + '/customer/get/' + this.user.data.email).subscribe(res => {
      if (res['success']) {
        // //console.log(res['data']['customers']);
        this.isAllcustomer = true;
        this.CUSTOMERS = res['data']['customers'];

        const result = [];
        const map = new Map();
        for (const item of this.CUSTOMERS) {
          if (!map.has(item.phoneno)) {
            map.set(item.phoneno, true);    // set any value to Map
            result.push(item);
          }
        }
        this.CUSTOMERS = result;
        //console.log(result)
        this.collectionSize = this.CUSTOMERS.length;
        this.CUSTOMERS.forEach((value, index) => {
          value.selected = false;
          value.id = index + 1
        });

      }
      else {
        // this.alertService.warning('No customers added yet!', false);
        this.CUSTOMERS = [];
        this.collectionSize = this.CUSTOMERS.length;
      }
    }, error => {
      // this.alertService.error('Error!', false);
    });
    this.isAll = false;
  }

  /**
   * 
   * @param content : model content
   * @param data  : holds the customer data for update
   * openUpdateCustomerModal () : This is for open the update customer model
   */
  openUpdateCustomerModal(content, data: any) {
    // //console.log(data);
    this.updateCustomerForm.patchValue(data);
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.submitted = false;
      this.updateCustomerForm.reset({
        _id: "",
        name: "",
        dialCode: "91",
        phoneno: "",
        email: "",
        occupation: "",
        birthdate: "",
        anniversary: "",
        address: "",
        campaignmsg: "",
        is_feedback: ""
      });
    });
  }

  /**
   * 
   * @param content : model content
   * @param customer : this variable holds the customer which is being to delete
   * openDeleteCustomerModal() : for open delete customer model for confirmation of 
   * delete customer i.e "Are you sure want to delete ? Y/N "
   */
  openDeleteCustomerModal(content, customer) {
    //console.log('cust=>', customer)
    this.customerId = customer._id
    //console.log('cust=>', this.customerId)
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

    })
  }

  /**
   * 
   * @param content : model content
   * openSelectedDeleteCustomerModal() : opens the delete customer model for 
   * confirmation from user(deleting multiple customers at a time)
   *  i.e "Are you sure want to delete ? Y/N "
   */
  openSelectedDeleteCustomerModal(content) {
    this.selectedCustomers = [];
    this.CUSTOMERS.forEach(value => {
      if (value.selected) {
        this.selectedCustomers.push(value);
      }
    });
    // console.log(this.selectedCustomers)
    if (this.selectedCustomers.length == 0) {
      this.alertService.warning('Select atleast one customer to delete!');
      return;
    } else {
      this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {

      })
    }
  }
  get cust() { return this.addCustomerForm.controls; }
  get custu() { return this.updateCustomerForm.controls; }

  /**
   * updateCustomer(): This function is for updating the customer details.
   *  if customer data/details updated successfully it gives message 
   * 'Customer updated successfully!'
   */
  updateCustomer() {
    this.submitted = true;
    if (this.updateCustomerForm.invalid) {
      return;
    }
    let data = { client_id: this.user.data._id, ...this.updateCustomerForm.value };
    // //console.log(data);
    this.http.put(this.baseUrl + '/customer/updatecustomer', data).subscribe(res => {
      // //console.log(res);
      if (res['success']) {
        this.alertService.success('Customer updated successfully!', false);
        this.getCustomers();
      } else {
        this.alertService.warning('Something went wrong!', false);
      }
    }, error => {
      // this.alertService.error('Error', false);
    });
    this.updateCustomerForm.reset({
      _id: "",
      name: "",
      dialCode: "91",
      phoneno: "",
      email: "",
      occupation: "",
      birthdate: "",
      anniversary: "",
      address: "",
      campaignmsg: "",
      is_feedback: ""
    });
    this.submitted = false;
    this.modalService.dismissAll();
  }

  /**
   * 
   * @param customer :
   * this function is for deleteing the customer from Database.
   * o/p : if deleted successfully it gives message 'Customer deleted successfully!', 
   * if not no message
   */
  deleteCustomer(customer: any) {
    // if (confirm('Do you really want to delete customer ' + '?')) {
    this.http.delete(this.baseUrl + '/customer/deletecustomer/' + this.user.data._id + '/' + this.customerId)
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.alertService.success('Customer deleted successfully!', false);
          this.getCustomers();
          this.modalService.dismissAll()
        } else {
          // this.alertService.warning('Something went wrong!', false);
        }
      }, error => {
        // this.alertService.error('Error!', false);
      });
    // }
  }

  modalDismiss() {
    this.modalService.dismissAll()
  }


  //uncomment later
  //tomorrow start from here
  /**
   * deleteSelected(): This function delete the multiple customers at a time
   * which is selected by user
   */
  deleteSelected() {
    this.selectedCustomers = [];
    this.CUSTOMERS.forEach(value => {
      if (value.selected) {
        this.selectedCustomers.push(value);
      }
    });
    // //console.log(this.selectedCustomers)
    if (this.selectedCustomers.length == 0) {
      this.alertService.warning('Select atleast one customer to delete!');
      return;
    } else {
      this.http.post(this.baseUrl + '/customer/deletecustomers/' + this.user.data._id, { customers: this.selectedCustomers })
        .subscribe(res => {
          // //console.log(res);
          if (res['success']) {
            const ele = document.getElementById("selectAllCheckbox") as HTMLInputElement;
            ele.checked = false;
            this.ngxService.start();
            setTimeout(() => {
              this.getCustomers();
              this.ngxService.stop();
            }, 3000);
            this.modalService.dismissAll()
            this.alertService.success('Customers deleted successfully!', false);
            this.selectedCustomers = [];
          } else {
            this.alertService.warning('Something went wrong!', false);
          }
        }, error => {
          // this.alertService.error('Error!', false);
        });
    }
    // //console.log(this.selectedCustomers);
    // delete api goes here...    
  }

  //update selected campaign 
  updateCampaignedSelected(status: boolean) {
    this.selectedCustomersCampaigned = [];
    this.CUSTOMERS.forEach(value => {
      if (value.selected) {
        this.selectedCustomersCampaigned.push(value._id);
      }
    });
    if (this.selectedCustomersCampaigned.length == 0) {
      this.alertService.warning('Select atleast one customer to update!');
      return;
    }
    // //console.log(this.selectedCustomers);
    // delete api goes here...
    this.http.post(this.baseUrl + '/customer/updatecampaigned/' + this.user.data._id + '/' + status, { customers: this.selectedCustomersCampaigned })
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          const ele = document.getElementById("selectAllCheckbox") as HTMLInputElement;
          ele.checked = false;
          this.ngxService.start();
          setTimeout(() => {
            this.getCustomers();
            this.alertService.success('Customers updated successfully!', false);
            this.ngxService.stop();
          }, 3000);
          this.selectedCustomersCampaigned = [];
        } else {
          this.alertService.warning('Something went wrong!', false);
        }
      }, error => {
        this.alertService.error('Error!', false);
      });
  }

  //update  feedback
  updateFeedbackedSelected(status: boolean) {
    this.selectedCustomersFeedbacked = [];
    this.CUSTOMERS.forEach(value => {
      if (value.selected) {
        this.selectedCustomersFeedbacked.push(value._id);
      }
    });
    if (this.selectedCustomersFeedbacked.length == 0) {
      this.alertService.warning('Select atleast one customer to update!');
      return;
    }
    this.http.post(this.baseUrl + '/customer/updatefeedbacked/' + this.user.data._id + '/' + status, { customers: this.selectedCustomersFeedbacked })
      .subscribe(res => {
        // //console.log(res);
        if (res['success']) {
          this.ngxService.start();
          setTimeout(() => {
            this.getCustomers();
            this.alertService.success('Customers updated successfully!', false);
            this.ngxService.stop();
            const ele = document.getElementById("selectAllCheckbox") as HTMLInputElement;
            ele.checked = false;
          }, 3000);
          this.selectedCustomersFeedbacked = [];
        } else {
          this.alertService.warning('Something went wrong!', false);
        }
      }, error => {
        this.alertService.error('Error!', false);
      });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => { }, (reason) => {
      this.submitted = false;
      this.addCustomerForm.reset({
        "name": "",
        "dialCode": "91",
        "phoneno": "",
        "email": "",
        "occupation": "",
        "birthdate": "",
        "anniversary": "",
        "address": ""
      });
    });
  }
/**
 * This function is used for add the new customer
 */
  addCustomer() {

    this.submitted = true;
    if (this.addCustomerForm.invalid) {
      return;
    }
    let email = '';
    if (this.user.data.role == "emp") {
      email = this.user.data.adminemail;
    } else {
      email = this.user.data.email;
    }
    let data = {
      email: email,
      client_id: this.user.data._id,
      customers: [this.addCustomerForm.value]
    };

    // //console.log(data);
    this.http.post(this.baseUrl + '/customer/post', data)
      .subscribe(res => {
        // this.isAllcustomer = false;
        // //console.log("res : ", res);
        if (res['success']) {
          this.addCustomerForm.reset({
            "name": "",
            "dialCode": "91",
            "phoneno": "",
            "email": "",
            "occupation": "",
            "birthdate": "",
            "anniversary": "",
            "address": ""
          });
          this.submitted = false;
          // this.isAllcustomer = true;
          this.alertService.success('Customer Added Successfully!', false);
          this.modalService.dismissAll();
          this.getCustomers();
        } else {
          // this.alertService.error("Can't insert!", false);
        }
      }, error => {
        // this.alertService.error("Error", false);
      });
  }
// to manage country chane event
  onCountryChange(obj) {
    // //console.log(obj);
    this.addCustomerForm.patchValue({ dialCode: obj.dialCode });
  }
  /**
   * This function is used for add the new customers in bulk
   */
  addCustomers() {
    let email = '';
    if (this.user.data.role == "emp") {
      email = this.user.data.adminemail;
    } else {
      email = this.user.data.email;
    }
    let data = {
      email: email,
      client_id: this.user.data._id,
      customers: this.IMPORTCUSTOMERS
    };
    this.http.post(this.baseUrl + '/customer/post', data)
      .subscribe(res => {
        //console.log(res);
        if (res['success']) {
          this.filename = '';
          this.alertService.success('Customers imported successfully!', true);
          this.IMPORTCUSTOMERS = [];
          this.importcollectionSize = this.IMPORTCUSTOMERS.length;
          this.router.navigate(['/home/customers']);
          this.validCsv = true;
                }
      }, error => {
        // this.alertService.error('Error!', false);
      });
  }
  absoluteIndex(indexOnPage: number): number {
    return this.itemsPerPage * (this.p - 1) + indexOnPage;
  }
  //To manage page change event
  pageChanged(event) {
    this.p = event;
  }
  change(index: number) {
    var actualindex = this.absoluteIndex(index);
    this.CUSTOMERS.forEach((d, index) => {
      if (actualindex == index) {
        if (this.CUSTOMERS[actualindex].selected) {
          this.CUSTOMERS[actualindex].selected = false
        } else {
          this.CUSTOMERS[actualindex].selected = true
        }
      }
    })
    // //console.log()
  }
  selectAll(flag) {
    this.isAll = !flag;
    if (this.isAll) {
      this.CUSTOMERS.forEach((cust, index) => {
        this.CUSTOMERS[index].selected = true;
      });
    } else {
      this.CUSTOMERS.forEach((cust, index) => {
        this.CUSTOMERS[index].selected = false;
      });
    }
  }
  /**
   * This function is used for getting sms service status
   * of user
   */
  getusersSmsservicestatus() {
    let emailid;
    if (this.user.data.role == 'emp') {
      emailid = this.user.data.adminemail;
    } else {
      emailid = this.user.data.email;
    }
    this.http.get(this.baseUrl + '/user/user/' + emailid).subscribe(res => {
      // //console.log("res  : ", res);
      if (res["success"]) {
        this.userservice.sms = res['data'].smscampaining;
        //console.log("smscampaining : ", this.userservice.sms)
      }
    })
  }
}
