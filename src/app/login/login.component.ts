import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  forgotPasswordForm: FormGroup;
  submitted: boolean = false;
  submittedF: boolean = false;
  returnUrl: string;
  loading: boolean = false;
  hide: boolean = true;
  constructor(
    public router: Router,
    public authenticationService: AuthenticationService,
    public route: ActivatedRoute,
    public alertService: AlertService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    this.forgotPasswordForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    })

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  
  get f() { return this.loginForm.controls; }

  /**
   * function to check login details
   */
  login() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.login((this.f.email.value).trim(), (this.f.password.value).trim())
      .pipe(first())
      .subscribe(
        data => {
          if (data.success === true) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.alertService.error('Invalid ' + data.message, false);
          }
        },
        error => {
          // this.alertService.error('Error', false);
          this.loading = false;
        });
  }

  forgotPassword() {
    this.hide = false;
  }

  get p() { return this.forgotPasswordForm.controls; }

  /**
   * Function for resetting the password by using mail
   */
  resetPassword() {
    this.submittedF = true;
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.authenticationService.resetPassword(this.p.email.value)
      .subscribe(data => {
        // console.log(data);
        if (data['success']) {
          this.alertService.success('Password sent on your Email-ID');
          this.hide = true;
        } else {
          this.alertService.error(data['msg'], false);
        }
      },
        error => {          
          this.loading = false;
      });
      this.forgotPasswordForm.reset({email: ""});
      this.submittedF = false;
  }

  goTologin() {
    this.hide = true;
  }

}
