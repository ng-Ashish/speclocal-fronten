import { Router } from '@angular/router';
import { Component, OnInit , Inject, NgZone } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { EasingLogic } from 'ng2-page-scroll';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.css']
})
export class WelcomeScreenComponent implements OnInit {

  user: any;
  submitted: boolean = false;
  baseUrl: string = environment.baseUrl;
  requestedContactForm: FormGroup;

  constructor(
    @Inject(DOCUMENT) public document: any, 
    public zone: NgZone,
    public router : Router, 
    public formBuilder: FormBuilder, 
    public modalService: NgbModal,
    public http: HttpClient,
    public alertService: AlertService) {
    zone.runOutsideAngular(() => {
      window.addEventListener('scroll', () => {
        const number = Math.min(this.document.body.scrollTop / 50, 1);
        // document.getElementById('navbar').style.background = `rgba(238,25,51,${number})`;
      });
    });
   }

  public myEasing: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
      // easeInOutExpo easing
      if (t === 0) {
        return b;
      }
      if (t === d) {
        return b + c;
      }
      if ((t /= d / 2) < 1) {
        return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      }
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
  };

  ngOnInit() {
    this.requestedContactForm = this.formBuilder.group({
      contacttype: [''],
      fullname: ['', [Validators.required,Validators.pattern(/^[a-zA-Z ]+$/)]],
      email: ['', Validators.required],
      businessname: ['', Validators.required],
      requirements: ['', Validators.required],
      contactno: ['',Validators.required],
      });
    // setTimeout(() => {
    //   this.router.navigate(['/login']);
    // }, 5000);  //5s
  }
  
  // login() {
  //   this.router.navigate(['/login']);
  // }
  openRequestedContact(requestedContactContent,type: string) {
    if (type == 'freedemo') {
      this.requestedContactForm.patchValue({ contacttype: 'freedemo' });
    } else {
      this.requestedContactForm.patchValue({ contacttype: 'getstarted' });     
    }

    this.modalService.open(requestedContactContent, { centered: true }).result.then((result) => {

    }, (reason) => { this.requestedContactForm.reset({ 
      contacttype:"",
      fullname : "",
      email : "" ,
      businessname  : "",
      requirements : "",
      contactno : "" }); 
    });
    this.submitted = false;
  }
  
  
  get form() { return this.requestedContactForm.controls; }

  onRequest(){
    this.submitted = true;
    if (this.requestedContactForm.invalid) {
      return;
    }
    // console.log(this.requestedContactForm.value);
    this.http.post(this.baseUrl + '/enquiry/addenquiry', this.requestedContactForm.value).subscribe(res =>{
      // console.log(res);
      if (res['success']) {
        this.alertService.success('Enquiry sent successfully!', false);
      } else {
        this.alertService.error('Something went wrong!', false);
      }
    }, error =>{
        // this.alertService.error('Error!', false);
    });
    this.requestedContactForm.reset({ 
      contacttype: "",
      fullname : "",
      email : "" ,
      businessname  : "",
      requirements : "",
      contactno : "" });
    this.submitted = false;
    this.modalService.dismissAll()
  }

}
