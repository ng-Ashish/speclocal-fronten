import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {

  public subscription: Subscription;
  message: any;

  constructor(public alertService: AlertService) { }

  ngOnInit() {
    this.subscription = this.alertService.getMessage().subscribe(message => {
      this.message = message;
      setTimeout(res =>{
        this.clearMessage();
      },5000);
    });
  }

  clearMessage() {
    this.message = undefined;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
