import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from '../services/alert.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  submitted: boolean = false;
  contactForm: FormGroup;
  baseUrl: string = environment.baseUrl;
  constructor(public alretService: AlertService, public http: HttpClient) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      subject: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required)
    });
  }

  get f() { return this.contactForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.contactForm.invalid) {
      this.alretService.warning('Something missing!', false);
      return;
    }    
    this.http.post(this.baseUrl + '/contactus/sendmail',this.contactForm.value)
    .subscribe(res=>{
      if (res['success']) {
        this.alretService.success('Thank you! Your message sent successfully!', false);
      } else {
        this.alretService.error('Something went wrong!', false);
      }
    }, error => {
      // this.alretService.error('Error!', false);
    });
    this.contactForm.reset({ name: "", email: "", subject: "", message: "" });
    this.submitted = false;
  }
}
