import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { AlertComponent } from './alert/alert.component';
import { FacebookModule } from 'ngx-facebook';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'ng2-file-upload';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { FileSaverModule } from 'ngx-filesaver';
import { ChartsModule } from 'ng2-charts';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
  GoogleApiModule,
  NgGapiClientConfig,
  NG_GAPI_CONFIG,
} from "ng-gapi";

import { AnalyticsComponent } from './home/analytics/analytics.component';
import { ReviewsComponent } from './home/reviews/reviews.component';
import { FeedbackComponent } from './home/feedback/feedback.component';
import { LocationsComponent } from './home/locations/locations.component';
import { LeaderboardComponent } from './home/leaderboard/leaderboard.component';
import { CustomersComponent } from './home/customers/customers.component';
import { CampaignComponent } from './home/campaign/campaign.component';
import { SettingsComponent } from './home/settings/settings.component';
import { AddComponent } from './home/campaign/add/add.component';
import { MyCampaignsComponent } from './home/campaign/my-campaigns/my-campaigns.component';
import { ProfilesettingsComponent } from './home/settings/profilesettings/profilesettings.component';
import { SupportComponent } from './home/support/support.component';
//old
let gapiClientConfig: NgGapiClientConfig = {  
  client_id: "442668123902-cflppjlfhghirrfpp9h5ogo79bqqkgd3.apps.googleusercontent.com",
  discoveryDocs: ["https://mybusiness.googleapis.com/$discovery/rest?version=v4"],
  scope: [
    "https://www.googleapis.com/auth/plus.business.manage",
  ].join(" ")
};

// let gapiClientConfig: NgGapiClientConfig = {
//   client_id: "881709144643-3utdrbj18keecnthvc8dpuu613lmii6k.apps.googleusercontent.com",
//   discoveryDocs: ["https://mybusiness.googleapis.com/$discovery/rest?version=v4"],
//   scope: [
//       "https://www.googleapis.com/auth/plus.business.manage",
//   ].join(" ")
// };

import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsComponent } from './terms/terms.component';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { ContactComponent } from './contact/contact.component';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule, SPINNER } from 'ngx-ui-loader';
import { LandingComponent } from './landing/landing.component';
import { EmployeesComponent } from './home/employees/employees.component';
import { GoogleAnalyticsComponent } from './home/google-analytics/google-analytics.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DatePipe } from '@angular/common';
import { FacebookAnalyticsComponent } from './home/facebook-analytics/facebook-analytics.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// import { FilterPipe } from './pipe/filter.pipe';
// import { EmpFilterPipe } from './pipe/empfilter.pipe';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    WelcomeScreenComponent,
    AlertComponent,
    AnalyticsComponent,
    ReviewsComponent,
    FeedbackComponent,
    LocationsComponent,
    LeaderboardComponent,
    CampaignComponent,
    CustomersComponent,
    SettingsComponent,
    AddComponent,
    MyCampaignsComponent,
    ProfilesettingsComponent,
    SupportComponent,
    PrivacypolicyComponent,
    TermsComponent,
    ContactComponent,
    LandingComponent,
    EmployeesComponent,    
    GoogleAnalyticsComponent,
    FacebookAnalyticsComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FacebookModule.forRoot(),
    NgbModule,
    NgxUiLoaderModule,
    NgxUiLoaderRouterModule,
    FileUploadModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
    UiSwitchModule,
    Ng2PageScrollModule.forRoot(),
    FileSaverModule,
    ChartsModule,
    Ng2TelInputModule,
    NgxPaginationModule,
    NgDatepickerModule,
    Ng2SearchPipeModule,
    InfiniteScrollModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
