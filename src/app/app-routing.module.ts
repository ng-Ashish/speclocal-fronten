import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AnalyticsComponent } from './home/analytics/analytics.component';
import { ReviewsComponent } from './home/reviews/reviews.component';
import { FeedbackComponent } from './home/feedback/feedback.component';
import { LeaderboardComponent } from './home/leaderboard/leaderboard.component';
import { LocationsComponent } from './home/locations/locations.component';
import { CustomersComponent } from './home/customers/customers.component';
import { CampaignComponent } from './home/campaign/campaign.component';
import { SettingsComponent } from './home/settings/settings.component';
import { MyCampaignsComponent } from './home/campaign/my-campaigns/my-campaigns.component';
import { AddComponent } from './home/campaign/add/add.component';

import { ProfilesettingsComponent } from './home/settings/profilesettings/profilesettings.component';
import { SupportComponent } from './home/support/support.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsComponent } from './terms/terms.component';
import { ContactComponent } from './contact/contact.component';
import { LandingComponent } from './landing/landing.component';
import { EmployeesComponent } from './home/employees/employees.component';
import { GoogleAnalyticsComponent } from './home/google-analytics/google-analytics.component';
import { FacebookAnalyticsComponent } from './home/facebook-analytics/facebook-analytics.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  // { path: '', component: WelcomeScreenComponent },
  { path: 'login', component: LoginComponent },
  { path: 'privacypolicy', component: PrivacypolicyComponent },
  { path: 'terms', component: TermsComponent },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'analytics', component: AnalyticsComponent },
      { path: 'googleanalytics', component: GoogleAnalyticsComponent },
      { path: 'reviews', component: ReviewsComponent },
      { path: 'feedback', component: FeedbackComponent },
      { path: 'leaderboard', component: LeaderboardComponent },
      { path: 'locations', component: LocationsComponent },
      { path: 'facebookanalytics', component: FacebookAnalyticsComponent },
      {
        path: 'customers',
        component: CustomersComponent
      },
      {
        path: 'campaign',
        component: CampaignComponent,
        children: [
          { path: '', redirectTo: 'add', pathMatch: 'full' },
          { path: 'add', component: AddComponent },
          { path: 'mycampaigns', component: MyCampaignsComponent }
        ]
      },
      {
        path: 'settings',
        component: SettingsComponent,
        children: [
          { path: 'profilesettings', component: ProfilesettingsComponent }
        ]
      },
      { path: 'support', component: SupportComponent },
      { path: 'employees', component: EmployeesComponent },
    ]
  },
  { path: 'contact', component: ContactComponent },
  { path: '**', redirectTo: "/home" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
