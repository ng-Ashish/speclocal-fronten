export const environment = {
  production: true,  
  baseUrl :  'https://speclocal.com:4500',
  // baseUrl :  'http://localhost:4500', // for local
  facebookreview :  "facebook/getreview",
  googlereview :  "google/getreview",
  zomatoreview :  "zomato/getreview",
  lastsevendaysfacebookreview : "facebook/getlastsevendayreview",
  lastsevendaysgooglereview   : "google/getlastsevendaysreview",
  lastfifteendaysfacebookreview : "facebook/getlast15daysreviews",
  lastfifteendaysgooglereview   : "google/getlast15daysreview",
  getanalyticdatabyday : "feedback/getanalyticsdatabyday",
  manualcron : 'facebook/manualCron'
};
