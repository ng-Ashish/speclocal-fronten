// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// const baseUrl =  'http://192.168.31.244:4500/';
export const environment = {
  production: false,  
  // baseUrl :  'http://localhost:4500', //local
  baseUrl :  'https://speclocal.com:4500', //live  
  facebookreview :  "facebook/getreview",
  googlereview :  "google/getreview",
  zomatoreview :  "zomato/getreview",
  lastsevendaysfacebookreview : "facebook/getlastsevendayreview",
  lastsevendaysgooglereview   : "google/getlastsevendaysreview",
  lastfifteendaysfacebookreview : "facebook/getlast15daysreviews",
  lastfifteendaysgooglereview   : "google/getlast15daysreview",
  getanalyticdatabyday : "feedback/getanalyticsdatabyday",
  manualcron : 'facebook/manualCron'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
